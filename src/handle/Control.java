package handle;

import java.net.InetAddress;

import link.UDPServer;
import dBase.DBConnection;
import dBase.DBRead;
import dBase.DBWrite;


public class Control {
	DBRead read;
	DBWrite write;
	public Control(){
		DBConnection dbConnection=new DBConnection();
		read=new DBRead(dbConnection);
		write=new DBWrite(dbConnection);
	}
	/**发送系统消息
	 * 
	 * @param title
	 * @param massage
	 * @throws Throwable
	 */
	public void sendSysMassage (String title,String massage) throws Throwable {
		String[] userIp=read.getstatusIpPort();
		for (int i = 0; i < userIp.length; i+=3) {
			massage+="<systemmassage><title:>"+title+"<massage:>"+massage+"<type:>1";
			massage = "[length=" + massage.length() + "]" + massage;
			UDPServer udp =new UDPServer(massage,InetAddress.getByName(userIp[i+1]),Integer.parseInt(userIp[i+2]));
			udp.send();
		}
		System.out.println("系统消息发送成功！");
	}
	/**系统添加用户
	 * userInfo{userId,psw,nickname,sex}
	 * @param userInfo
	 */
	public void addNewUserId(String[] userInfo){
		write.setLogin(userInfo[0], userInfo[1], userInfo[2]);
		System.out.println("成功添加用户"+userInfo[0]);
		
	}
	/**系统修改用户状态
	 * 
	 * @param userInfo{userId,pwd,nickname,status}
	 */
	public void updateUserType(String[] userInfo) {
		write.setStatus(userInfo[0], Integer.parseInt(userInfo[3]));
		write.setUserlnfo_control(userInfo[0], userInfo[1], userInfo[2]);
		System.out.println("成功修改用户状态"+userInfo[0]);
	}
	
}
