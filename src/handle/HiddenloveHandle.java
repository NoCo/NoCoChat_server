package handle;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;

import link.UDPServer;
import dBase.DBRead;
import dBase.DBWrite;
import exception.ConfigurationException;
import file.Get;

public class HiddenloveHandle {
	Get get = null;
	DBWrite write = null;
	DBRead read = null;

	/**
	 * 构造函数
	 * 
	 * @param get
	 * @param write
	 * @param read
	 * */
	public HiddenloveHandle(Get get, DBWrite write, DBRead read) {
		this.get = get;
		this.write = write;
		this.read = read;
	}

	/**
	 * 接口方法
	 * 
	 * @param value
	 * @param command
	 * @return
	 * @throws Throwable
	 */
	public String get(List<String> value, String command) throws Throwable {
		String[] str = getCommand();
		String valueString[] = value.toArray(new String[] {});
		if (command.equals(str[0]) || command == str[0]) {
			// 添加暗恋对象
			String temp = getHiddenLove(valueString);
			return temp;
		} else if (command.equals(str[1]) || command == str[1]) {
			// 获取所有暗恋
			String temp = getMyHiddenLove(valueString);
			return temp;
		} else if (command.equals(str[2]) || command == str[2]) {
			// 取消暗恋
			String temp = deleteHiddenLove(valueString);
			return temp;
		} else {
			return null;
		}
	}

	/**
	 * 获取所有暗恋对象
	 * 
	 * @param valueString
	 * @return
	 */
	private String getMyHiddenLove(String[] valueString) {
		String fromuserId = valueString[1];
		String[] myHL = read.getmyhiddenlove(fromuserId);
		String temp = "";
		List<String> output = new ArrayList<String>();
		if (myHL.length > 0) {
			for (int i = 0; i < myHL.length; i++) {
				String[] lovestatus = read.getUserlnfo(myHL[i]);
				String str = "<userid:>" + myHL[i] + "<username:>"
						+ read.getNickname(myHL[i]) + "<lovestatus:>"
						+ lovestatus[2] + "<count:>"
						+ read.getOtherHiddenLoveCount(myHL[i]);
				output.add(str);
			}
			String[] str = output.toArray(new String[] {});
			for (int i = 0; i < str.length; i++) {
				temp += str[i];
			}
			temp = "<getyourhiddenlove>" + temp;
			temp = "[length=" + temp.length() + "]" + temp;
		}
		return temp;
	}

	/**
	 * 取消暗恋对象
	 * 
	 * @param valueString
	 * @return
	 */
	private String deleteHiddenLove(String[] valueString) {
		String userId = valueString[1];// 被暗恋者id
		String fromuserId = valueString[3];// 自己id
		write.deleteHiddenlove(userId, fromuserId);
		String output = "<deletehiddenlovesuccessful><userid:>" + userId;
		output = "[length=" + output.length() + "]" + output;
		return output;
	}

	/**
	 * 添加暗恋对象
	 * 
	 * @param valueString
	 * @return
	 * @throws Throwable
	 */
	private String getHiddenLove(String[] valueString) throws Throwable {
		String userId = valueString[1];// 被暗恋者id
		String fromuserId = valueString[3];// 自己id
		write.setHiddenLove(userId, fromuserId);
		String output = "<hiddenlovesuccessful><userid:>" + userId;
		String output_1="";
		output = "[length=" + output.length() + "]" + output;
		if (read.isHiddenOk(userId, fromuserId)
				&& read.isHiddenOk(fromuserId, userId)) {
			if (!read.isFriends(userId, fromuserId)) {
				String[] ip = read.getIPPort(userId);
				output_1 = "<congratuatehiddenlove><type:>1<userid:>"
						+ userId + "<username:>" + read.getNickname(userId);
				if (read.getStatus(userId) == 0) {
					write.setSystemMassage(userId, "恭喜你们配对成功", "恭喜你与"
							+ fromuserId + read.getNickname(fromuserId)
							+ "配对成功", "1");
				} else {
					InetAddress address = InetAddress.getByName(ip[0]);
					String outStr = "<congratuatehiddenlove><type:>1"
							+ "<userid:>" + fromuserId + "<username:>"
							+ read.getNickname(fromuserId);
					outStr = "[length=" + outStr.length() + "]" + outStr;
					UDPServer udp = new UDPServer(outStr, address,
							Integer.parseInt(ip[1]));
					udp.send();
				}
			} else {
				String[] ip = read.getIPPort(userId);
				String[] ip_2 = read.getIPPort(fromuserId);
				output_1 = "<congratuatehiddenlove><type:>0<userid:>"
						+ userId + "<username:>" + read.getNickname(userId)
						+ "<ip:>" + ip[0] + "<port:>" + ip[1];
				write.setFriends(fromuserId, userId, read.getNickname(userId),
						ip_2[0], ip_2[1], ip[0], ip[1],
						read.getNickname(fromuserId));
				if (read.getStatus(userId) == 0) {
					write.setSystemMassage(userId,  "恭喜您暗恋的用户:"
							+ fromuserId + read.getNickname(fromuserId)
							+ "也暗恋你，系统已将该用户添加为您的好友！", "恭喜您暗恋的用户:"
							+ fromuserId + read.getNickname(fromuserId)
							+ "也暗恋你，系统已将该用户添加为您的好友！", "2");
				} else {
					InetAddress address = InetAddress.getByName(ip[0]);
					String outStr = "<congratuatehiddenlove><type:>0"
							+ "<userid:>" + fromuserId + "<username:>"
							+ read.getNickname(fromuserId) + "<ip:>" + ip_2[0]
							+ "<port:>" + ip_2[1];
					outStr = "[length=" + outStr.length() + "]" + outStr;
					UDPServer udp = new UDPServer(outStr, address,
							Integer.parseInt(ip[1]));
					udp.send();
				}
			}
		}
		output_1 = "[length=" + output_1.length() + "]" + output_1;
		return output+output_1;
	}

	/**
	 * 获取操作命令
	 * 
	 * @return
	 */
	private String[] getCommand() {
		try {
			String[] command = get.getHiddenlove();
			return command;
		} catch (ConfigurationException e) {
			e.printStackTrace();
		}
		return null;
	}

}
