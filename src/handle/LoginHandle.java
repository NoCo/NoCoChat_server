package handle;

import java.util.List;

import dBase.DBRead;
import dBase.DBWrite;
import dbClass.UserInfo;
import exception.ConfigurationException;
import file.Get;

/**注册处理类
 * 
 * @author jancojie
 *
 */
public class LoginHandle {
	Get get = null;
	DBWrite write=null;
	DBRead read=null;
	
	public LoginHandle(Get get, DBWrite write, DBRead read) {
		this.get = get;
		this.write = write;
		this.read = read;
	}
	/**命令分发
	 * 
	 * @param value
	 * @param command
	 * @return
	 * @throws Throwable
	 */
	public String getLogin(List<String> value, String command) throws Throwable {
		String[] str=getCommand();
		String valueString[] = value.toArray(new String[] {});
		if(command.equals(str[0])||command==str[0]){
			//用户注册;
			boolean falg=setlogInUser(valueString);
			if(falg){
				String temp="<loginuserresult><type:>1";
				temp="[length="+temp.length()+"]"+temp;
				return temp;
			}
			else {
				String temp="<loginuserresult><type:>0";
				temp="[length="+temp.length()+"]"+temp;
				return temp;
			}
		}else if (command.equals(str[1])||command==str[1]) {
			//聊天室注册；
				String temp=setlogInTroom(valueString);
				temp="[length="+temp.length()+"]"+temp;
				return temp;
		}
		return null;
		
		
		
	 } 
	
	/**取得命令
	 * 
	 * @return
	 */
	private String[] getCommand() {
		try {
			String[] command=get.getLogin();
			return command;
		} catch (ConfigurationException e) {
				e.printStackTrace();
		}
		return null;
	}
	
	/**判断用户是否存在
	 * 
	 * @param userId
	 * @return
	 */
	private boolean isUserInfo(String userId){
		String []str=read.getUserInfoId();
		for (int i = 0; i < str.length; i++) {
			if(userId.equals(str[i])||userId==str[i]) {
				return false;
			}
		}
		return true;
	}
	
	/**判断聊天室是否存在
	 * 
	 * @param gId
	 * @return
	 */
	private boolean isUserGroup(String gId){
		String []str=read.getusergroup_findGid(gId);
		for (int i = 0; i < str.length; i++) {
			if(str.length>0) {
				return false;
			}
		}
		return true;
	}
	
	/**创建聊天室
	 * 
	 * @param valueString
	 * @return
	 */
	private String setlogInTroom(String[] valueString) {
		String userId=valueString[1];
		String gId=valueString[3];
		String gname=valueString[5];
		String output="";
		if(isUserGroup(gId)){
			output="<logintalkroomresult><type:>1<talkroomid:>"
					+ gId+"<talkroomname:>"+gname;
			write.setUserGroup(userId, gname, gId);
		}
		else {
			output="<logintalkroomresult><type:>0"
					+ "<talkroomname:>"+gname;
		}
		return output;
	}
	
	/**创建用户
	 * 
	 * @param valueString
	 * @return
	 * @throws Throwable
	 */
	private boolean setlogInUser(String[] valueString) throws Throwable {
		 UserInfo user = new UserInfo();
		 user.setUserId(valueString[1]);
		 user.setNickname(valueString[3]);
		 user.setPwd(valueString[5]);
		 if(isUserInfo(user.getUserId())){
		 write.setLogin(user.getUserId(), user.getPwd(),user.getNickname());
		 	return true;
		 }
		 else {
			return false;
		}
		
	}
}
