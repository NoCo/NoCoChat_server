package handle;

import java.net.InetAddress;
import java.util.List;

import link.UDPServer;
import dBase.DBRead;
import dBase.DBWrite;
import exception.ConfigurationException;
import file.Get;
/**下线操作类
 * 
 * @author ASIMO
 *
 */
public class DownLineHandle {
	Get get = null;
	DBWrite write=null;
	DBRead read=null;
	
	public DownLineHandle(Get get, DBWrite write, DBRead read) {
		this.get = get;
		this.write = write;
		this.read = read;
	}
	
	/**接口方法
	 * 
	 * @param value
	 * @param command
	 * @return
	 * @throws Throwable
	 */
	public String getDownLine(List<String> value, String command) throws Throwable {
		String[] str=getCommand();
		String valueString[] = value.toArray(new String[] {});
		if(command.equals(str[0])||command==str[0]){
			//下线
			if(downLine(valueString)){
				return "";
			}
			
		}

		return null;
	}
	
	/**取得操作命令
	 * 
	 * @return
	 */
	private String[] getCommand() {
		try {
			String[] command=get.getDownLine();
			return command;
		} catch (ConfigurationException e) {
				e.printStackTrace();
		}
		return null;
	}
	
	/**下线
	 * 
	 * @param valuetring
	 * @return
	 * @throws Throwable
	 */
	private boolean downLine(String[] valuetring) throws Throwable{
		String userId=valuetring[1];
		 if(write.setDownLine(userId)){
				sendDownLineToFriends(userId);
				sendDownLineTogFriends(userId);
		 return true;
		 }
		 else {
			return false;
		}
	}
	
	/**发送离线消息给好友
	 * 
	 * @param userId
	 * @throws Throwable
	 */
	private void sendDownLineToFriends(String userId) throws Throwable {
	
		InetAddress address=null;
		UDPServer udpServer=null;
		String[] friends=read.getFriends(userId);
		for (int i = 0; i < friends.length; i+=5) {
			String fid=friends[i];
			if(isStatus(fid)){
				String fip=friends[i+4];
				address=InetAddress.getByName(fip);
				int fport=Integer.parseInt(friends[i+1]);
				String out="<youfrienddownline><userid:>"+
				userId+"<name:>"+read.getNickname(userId)+"<ip:>_empty_<port:>0";
				out="[length="+out.length()+"]"+out;
				String outStr="<systemmassage><title:>SiseChat<massage:>您的好友:"+
						read.getNickname(userId)+" 已离线！<type:>1";
				outStr = "[length=" + outStr.length() + "]" + outStr;
				udpServer=new UDPServer(out+outStr, address, fport);
				udpServer.send();
			}
		}
		
	}
	
	/**发送离线信息给群好友
	 * 
	 * @param userId
	 * @throws Throwable
	 */
	private void sendDownLineTogFriends(String userId) throws Throwable {
		
		InetAddress address=null;
		UDPServer udpServer=null;
		String name=read.getNickname(userId);
		String[] groud = read.getMYUserGroupList(userId);
		for (int i = 0; i < groud.length; i+=2) {
			String[] fid=read.getusergroupFriend_userId(groud[i]);
			for (int j = 0; j < fid.length; j++) {
				String[] fip=read.getIPPort(fid[j]);
				if(fid[j].equals(userId)){
					continue;
				}
				if(isStatus(fid[j])){
				address=InetAddress.getByName(fip[0]);
				int fport=Integer.parseInt(fip[1]);
				String out="<yougfrienddownline><talkroomid>"+groud[i]+"<userid:>"+userId+"<name:>"+name+"<ip:>_empty_<port>0";
				out="[length="+out.length()+"]"+out;
				udpServer=new UDPServer(out, address, fport);
				udpServer.send();
			}
		}
	}
}
	
	/**判断好友是否在线
	 * @param userId
	 * @return
	 */
	private boolean isStatus(String userId){
	
		int i=read.getStatus(userId);
		if(i==1){
			return true;
		}else {
			return false;
		}
	}
}
