package handle;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;

import link.UDPServer;
import dBase.DBRead;
import dBase.DBWrite;
import exception.ConfigurationException;
import file.Get;

public class GetMessage {
	Get get = null;
	DBWrite write = null;
	DBRead read = null;

	/**
	 * 构造函数
	 * 
	 * @param get
	 * @param write
	 * @param read
	 */
	public GetMessage(Get get, DBWrite write, DBRead read) {
		this.get = get;
		this.write = write;
		this.read = read;
	}

	/**
	 * 接口方法
	 * 
	 * @param value
	 * @param command
	 * @return
	 * @throws Throwable
	 */
	public String get(List<String> value, String command) throws Throwable {
		String[] str = getCommand();
		String valueString[] = value.toArray(new String[] {});
		if (command.equals(str[0]) || command == str[0]) {
			// 添加好友缓存
			String temp_1[] = getFriendCache(valueString);
			String temp_2[] = getBackFriendCache(valueString);
			for (int i = 0; i < temp_1.length; i++) {
				temp_1[i] = "[length=" + temp_1[i].length() + "]" + temp_1[i];
			}
			for (int i = 0; i < temp_2.length; i++) {
				temp_2[i] = "[length=" + temp_2[i].length() + "]" + temp_2[i];
			}
			String[] temp_3 = new String[temp_1.length + temp_2.length];
			System.arraycopy(temp_1, 0, temp_3, 0, temp_1.length);
			System.arraycopy(temp_2, 0, temp_3, temp_1.length, temp_2.length);
			String temp = "";
			for (int i = 0; i < temp_3.length; i++) {
				temp += temp_3[i];
			}
			return temp;
		} else if (command.equals(str[1]) || command == str[1]) {
			// 发送离线信息缓存
			String temp = getChatInfo(valueString);
			return temp;
		} else if (command.equals(str[2]) || command == str[2]) {
			// 离线信息缓存
			String temp = setChatInfo(valueString);
			temp = "[length=" + temp.length() + "]" + temp;
			return temp;
		} else if (command.equals(str[3]) || command == str[3]) {
			// 聊天室成员添加缓存
			String temp_1[] = getgFriendCache(valueString);
			String temp_2[] = getBackgFriendCache(valueString);
			for (int i = 0; i < temp_1.length; i++) {
				temp_1[i] = "[length=" + temp_1[i].length() + "]" + temp_1[i];
			}
			for (int i = 0; i < temp_2.length; i++) {
				temp_2[i] = "[length=" + temp_2[i].length() + "]" + temp_2[i];
			}
			String[] temp_3 = new String[temp_1.length + temp_2.length];
			System.arraycopy(temp_1, 0, temp_3, 0, temp_1.length);
			System.arraycopy(temp_2, 0, temp_3, temp_1.length, temp_2.length);
			String temp = "";
			for (int i = 0; i < temp_3.length; i++) {
				temp += temp_3[i];
			}
			return temp;
		} else if (command.equals(str[4]) || command == str[4]) {
			// 发送系统信息
			String temp = getSystemMassage(valueString);
			return temp;
		} else {
			return null;
		}
	}

	/**
	 * 发送系统信息
	 * 
	 * @param valueString
	 * @return
	 */
	private String getSystemMassage(String[] valueString) {
		String userId = valueString[1];
		String[] systemMassage = read.getSystemMassage(userId);
		List<String> output = new ArrayList<String>();
		for (int i = 0; i < systemMassage.length; i += 3) {
			if (systemMassage[i + 2].equals("0") || systemMassage[i + 2] == "0") {
				String[] foundthing = read
						.getfoundthing_per(systemMassage[i + 1]);
				if (foundthing.length == 0) {
					continue;
				}
				String found = "<setfoundthing><title:>" + foundthing[1]
						+ "<type:>" + foundthing[2] + "<cardnumber:>"
						+ foundthing[3] + "<cardname:>" + foundthing[4]
						+ "<describption:>" + foundthing[5] + "<time:>"
						+ foundthing[6] + "<telephone:>" + foundthing[7]
						+ "<userid:>" + foundthing[8] + "<id:>" + foundthing[0];
				found = "[length=" + found.length() + "]" + found;
				String lostthingfound = "<systemmassage><title:>"
						+ systemMassage[i] + "<massage:>"
						+ systemMassage[i + 1] + "<type:>"
						+ systemMassage[i + 2];
				lostthingfound = "[length=" + lostthingfound.length() + "]"
						+ lostthingfound;
				output.add(found + lostthingfound);
			} else {
				String massage = "<systemmassage><title:>" + systemMassage[i]
						+ "<massage:>" + systemMassage[i + 1] + "<type:>"
						+ systemMassage[i + 2];
				massage = "[length=" + massage.length() + "]" + massage;
				output.add(massage);
			}
		}
		write.deleteSystemMassage(userId);
		String[] str = output.toArray(new String[] {});
		String out = "";
		for (int i = 0; i < str.length; i++) {
			out += str[i];
		}
		return out;
	}

	/**
	 * 聊天室成员添加返回缓存
	 * 
	 * @param valueString
	 * @return
	 */
	private String[] getBackgFriendCache(String[] valueString) {
		String userId = valueString[1];
		String[] getgFriendMessage = read.getBackgFriendCache(userId);
		List<String> output = new ArrayList<String>();
		for (int i = 0; i < getgFriendMessage.length; i += 6) {
			String fuserId = getgFriendMessage[i+1];
			String[] fip = read.getIPPort(fuserId);
		if(getgFriendMessage[i+5].equals("0")){
				if (getgFriendMessage[i + 2].equals("1")
						|| getgFriendMessage[i + 2] == "1") {
					output.add("<addtalkroomfriendback><fromid:>" + fuserId
							+ "<fromname:>" + read.getNickname(fuserId)
							+ "<type:>1<ip:>" + fip[0] + "<port:>" + fip[1]
							+ "<talkroomid:>" + getgFriendMessage[i + 3]);
				} else {
					output.add("<addtalkroomfriendback><fromid:>" + fuserId
							+ "<fromname:>" + read.getNickname(fuserId)
							+ "<type:>0<talkroomid:>" + getgFriendMessage[i + 3]);
				}
			}else {
				if (getgFriendMessage[i + 2].equals("1")
						|| getgFriendMessage[i + 2] == "1") {
					output.add("<addtalkroominviteback>"+
							"<talkroomid:>"+getgFriendMessage[i + 3]+
							"<talkroomname:>"+getgFriendMessage[i + 4]+
							"<fromuserid:>"+fuserId+"<type:>1");
				} else {
					output.add("<addtalkroominviteback>"+
							"<talkroomid:>"+getgFriendMessage[i + 3]+
							"<talkroomname:>"+getgFriendMessage[i + 4]+
							"<fromuserid:>"+fuserId+"<type:>0");
				}
			}
		}
		write.deleteBackgFriendCache(userId);
		String str[] = output.toArray(new String[] {});
		return str;
	}

	/**
	 * 聊天室成员添加缓存
	 * 
	 * @param valueString
	 * @return
	 * @throws Throwable
	 */
	private String[] getgFriendCache(String[] valueString) throws Throwable {
		String fuserId = valueString[1];
		String[] getgFriendMessage = read.getGetgFriend(fuserId);
		List<String> output = new ArrayList<String>();
		for (int i = 0; i < getgFriendMessage.length; i += 5) {
			if (getgFriendMessage[i + 4].equals("0")) {
				output.add("<addtalkroomfriendyou><fromid:>"
						+ getgFriendMessage[i] + "<fromname:>"
						+ read.getNickname(getgFriendMessage[i])
						+ "<talkroomid:>" + getgFriendMessage[i + 2]
						+ "<talkroomname:>" + getgFriendMessage[i + 3]);
			} else {
				String ip[] = read.getIPPort(getgFriendMessage[i]);
				output.add("<addtalkroominvite><userid:>"
						+ getgFriendMessage[i] + "<name:>"
						+ read.getNickname(getgFriendMessage[i])
						+ "<talkroomid:>" + getgFriendMessage[i + 2] + "<ip:>"
						+ ip[0] + "<port:>" + ip[1]);
			}
		}
		write.deleteGetgFriends(fuserId);
		String str[] = output.toArray(new String[] {});
		return str;
	}

	/**
	 * 添加好友缓存
	 * 
	 * @param valueString
	 * @return
	 * @throws Throwable
	 */
	private String[] getFriendCache(String[] valueString) throws Throwable {
		String fuserId = valueString[1];
		String[] getFriendMessage = read.getGetFriend(fuserId);
		write.deleteGetFriends(fuserId);
		List<String> output = new ArrayList<String>();
		for (int i = 0; i < getFriendMessage.length; i += 2) {
			String[] ip = read.getIPPort(getFriendMessage[i]);
			output.add("<addyou><fromuserid:>" + getFriendMessage[i]
					+ "<fromname:>" + getFriendMessage[i + 1] + "<ip:>" + ip[0]
					+ "<port:>" + ip[1]);
		}
		String str[] = output.toArray(new String[] {});
		return str;
	}

	/**
	 * 添加好友返回缓存
	 * 
	 * @param valueString
	 * @return
	 */
	private String[] getBackFriendCache(String[] valueString) {
		String userId = valueString[1];
		String[] getFriendMessage = read.getBackFriendCache(userId);
		
		List<String> output = new ArrayList<String>();
		for (int i = 0; i < getFriendMessage.length; i += 2) {
			String fuserId = getFriendMessage[i];
			String[] fip = read.getIPPort(fuserId);
			String[] ip=read.getIPPort(userId);
			if (getFriendMessage[i + 1].equals("1")
					|| getFriendMessage[i + 1] == "1") {
					write.setFriends(userId, fuserId, read.getNickname(fuserId), fip[0], fip[1], ip[0],
							ip[1], read.getNickname(userId));
				output.add("<adduserfriendback>" + "<fromname:>"
						+ read.getNickname(fuserId) + "<fromuserid:>" + fuserId
						+ "<type:>1<ip:>" + fip[0] + "<port:>" + fip[1]);
			} else {
				output.add("<adduserfriendback>" + "<fromname:>"
						+ read.getNickname(fuserId) + "<fromuserid:>" + fuserId
						+ "<type:>0");
			}
			write.deleteBackFriendCache(userId);

		}
		String str[] = output.toArray(new String[] {});
		return str;
	}

	/**
	 * 离线信息缓存
	 * 
	 * @param valueString
	 * @return
	 * @throws Throwable
	 */
	private String getChatInfo(String[] valueString) throws Throwable {
		String fId = valueString[1];
		String infoCache[] = read.getInfoCache(fId);
		String out = "";

		for (int i = 0; i < infoCache.length; i += 5) {
			out = "<send><message:>" + infoCache[i] + "<time:>"
					+ infoCache[i + 1] + "<userid:>" + fId + "<name:>"
					+ infoCache[i + 2] + "<fromuserid:>" + infoCache[i + 3]
					+ "<fromname:>" + infoCache[i + 4];
			out = "[length=" + out.length() + "]" + out;
			String[] ipPort = read.getIPPort(fId);
			InetAddress address = InetAddress.getByName(ipPort[0]);
			UDPServer udp = new UDPServer(out, address,
					Integer.parseInt(ipPort[1]));
			udp.send();
			write.deleteChatInfo(fId);
		}

		return null;
	}

	/**
	 * 缓存离线信息
	 * 
	 * @param valueString
	 * @return
	 */
	private String setChatInfo(String[] valueString) {
		boolean flag = write
				.setChatInfo(valueString[1], valueString[3], valueString[9],
						valueString[11], valueString[5], valueString[7]);
		String out = "";
		if (flag) {
			out = "<sendok>";
		} else {
			out = "<sendunok>";
		}

		return out;

	}

	/**
	 * 获取操作命令
	 * 
	 * @return
	 */
	private String[] getCommand() {
		try {
			String[] command = get.getMessage();
			return command;
		} catch (ConfigurationException e) {
			e.printStackTrace();
		}
		return null;
	}

}
