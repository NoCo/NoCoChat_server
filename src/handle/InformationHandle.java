package handle;

import java.sql.SQLException;
import java.util.List;

import dBase.DBRead;
import dBase.DBWrite;
import exception.ConfigurationException;
import file.Get;
/**信息处理类
 * 
 * @author jancojie
 *
 */
public class InformationHandle {
	Get get = null;
	DBWrite write=null;
	DBRead read=null;
	
	public InformationHandle(Get get, DBWrite write, DBRead read) {
		this.get = get;
		this.write = write;
		this.read = read;
	}
	
	/**命令分发(接口方法)
	 * 
	 * @param value
	 * @param command
	 * @return
	 * @throws Throwable
	 */
	public String getInformation(List<String> value, String command) throws Throwable {
		String[] str=getCommand();
		String valueString[] = value.toArray(new String[] {});
		if(command.equals(str[0])||command==str[0]){
			//取得个人信息
			String temp=getUserInfo(valueString[1]);
			temp="[length="+temp.length()+"]"+temp;
			return temp;
		}else if (command.equals(str[1])||command==str[1]) {
			//取得他人信息
			String temp=getotherinfo(valueString[1]);
			temp="[length="+temp.length()+"]"+temp;
			return temp;
		}
		else if (command.equals(str[2])||command==str[2]) {
			//设置用户信息
			String temp=setUserInfo(valueString);
			temp="[length="+temp.length()+"]"+temp;
			return temp;
		}else if (command.equals(str[3])||command==str[3]) {
			//设置好友备注名；
			String temp=setExtraName(valueString);
			return temp;
		}else if (command.equals(str[4])||command==str[4]) {
			//创建好友分组；
			String temp=cratesName(valueString);
			return temp;
		}else if (command.equals(str[5])||command==str[5]) {
			//修改好友分组名；
			String temp=setsName(valueString);
			return temp;
		}else if (command.equals(str[6])||command==str[6]) {
			//修改好友分组；
			String temp=setFriendsName(valueString);
			return temp;
		}
		
		return null;
	}
	
	/**修改好友分组
	 * 
	 * @param valueString
	 * @return
	 */
	private String setFriendsName(String[] valueString) {
		String userId=valueString[1];
		String fId=valueString[3];
		String sname=valueString[5];
		String snamenew=valueString[7];
		write.updateFriendsname(userId, fId, sname, snamenew);
		return null;
	}
	
	/**修改好友分组名
	 * 
	 * @param valueString
	 * @return
	 */
	private String setsName(String[] valueString) {
		String userId=valueString[1];
		String sname=valueString[3];
		String snamenew=valueString[5];
		write.updateFriendGroup(userId, sname,snamenew);
		return null;
	}
	
	/**创建好友分组
	 * 
	 * @param valueString
	 * @return
	 */
	private String cratesName(String[] valueString) {
		String userId=valueString[1];
		String sname=valueString[3];
		write.insertFriendGroup(userId, sname);
		return null;
	}
	
	/**设置好友备注名
	 * 
	 * @param valueString
	 * @return
	 */
	private String setExtraName(String[] valueString) {
		String userId=valueString[1];
		String fId=valueString[3];
		String extraname=valueString[5];
		write.updateFriendname(userId, fId, extraname);
		return null;
	}
	
	/**取得个人信息
	 * 
	 * @param userId
	 * @return
	 * @throws SQLException
	 */
	private String getUserInfo(String userId) throws SQLException{
		String[] userInfo=read.getUserlnfo(userId);
		String[] mysise=read.getMysise(userId);
		String[] email=read.getEmail(userId);
		String output="<getmyinformation><sign:>"+userInfo[0]+"<name:>"
				+userInfo[1]+"<lovestatus:>"+userInfo[2]+"<sex:>"+userInfo[3]+
				"<personal:>"+Integer.parseInt(userInfo[4])+
				"<major:>"+mysise[0]+"<realname:>"+mysise[1]+"<mysiseid:>"+mysise[2]
				+ "<class:>"+mysise[3]+"<department:>"+mysise[4]
				+ "<email:>"+email[0]+"<password:>"+email[1];
		return output;
		
	}
	
	/**取得他人信息
	 * 
	 * @param userId
	 * @return
	 */
	private String getotherinfo(String userId) {
		int personal=read.getPersonal(userId);
		String input="";
		String[] userInfo=read.getUserlnfo(userId);
		String[] mysise=read.getMysise(userId);
		String[] email=read.getEmail(userId);
		if(personal==1){
			input="<getotherinformation><userid:>"+userId+
					"<sign:>"+userInfo[0]+"<name:>"
					+ userInfo[1]+"<lovestatus:>"+userInfo[2]+
					"<sex:>"+userInfo[3]+"<major:>"+mysise[0]+
					"<realname:>"+mysise[1]+"<class:>"+mysise[3]+
					"<deparment:>"+mysise[4]+"<email:>"+email[0];
		}
		else{
			input="<getotherinformation><userid:>"
					+ userId+"<sign:>"+userInfo[0]+"<name:>"
							+ userInfo[1]+"<lovestatus:>"+ 
					userInfo[2]+"<sex:>"+userInfo[3];
		}
		return input;
		
	}
	
	/**设置个人信息
	 * 
	 * @param input
	 * @return
	 */
	private String setUserInfo(String[] input) {
		String output="";
		boolean set_1=write.setUserlnfo(input[1],input[3], input[5], 
				Integer.parseInt(input[7]),Integer.
				parseInt(input[9]),Integer.parseInt(input[11]));
		boolean set_2=write.setmysise(input[1], input[13], input[15],
				input[17], input[19], input[21]);
		boolean set_3=write.setEmail_update(input[1], input[23]);
		if(set_1&&set_2&&set_3){
			 output="<changemyinformationok>";
		}
		else {
			 output="<changemyinformationunok>";
		}
		return output;
	}
	
	/**取得命令
	 * 
	 * @return 命令
	 */
	private String[] getCommand() {
		try {
			String[] command=get.getInformation();
			return command;
		} catch (ConfigurationException e) {
				e.printStackTrace();
		}
		return null;
	}
	
}
