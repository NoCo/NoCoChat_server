package handle;


import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;




import dBase.DBConnection;
import dBase.DBRead;
import dBase.DBWrite;
import file.Get;
/**处理分发类
 * 
 * @author ASIMO
 *
 */
public class Prehandle {
	Get get = new Get();
	List<String> input = new ArrayList<String>();
	
	/**
	 * handle分发
	 * 
	 * @param data
	 * @return
	 * @throws Throwable
	 */
	public byte[] assignHandle(byte[] data) throws Throwable {
		DBConnection dbConnection=new DBConnection();
		DBWrite write=new DBWrite(dbConnection);
		DBRead read=new DBRead(dbConnection);
		String str=new String(data,"UTF-8");
		String[] logIn=get.getLogin();
		String[] signIn=get.getSignIn();
		String[] add=get.getAdd();
		String[] friendsList=get.getFriendsList();
		String[] downline=get.getDownLine();
		String[] mysise=get.getMysise();
		String[] email=get.getEmail();
		String[] information=get.getInformation();
		String[] file=get.getFile();
		String[] message=get.getMessage();
		String[] publice=get.getPublic();
		String[] hiddenlove=get.getHiddenlove();
		List<String> outpList = new ArrayList<String>();
		outpList = changeString(str,null);
		String srt[] = outpList.toArray(new String[] {});
		String outtemp="";
		for (int i = 0; i < srt.length; i++) {
			String temp[] = getCommande(srt[i]);
			input=getValue(temp[1]);
			if (isCommand(temp[0], logIn)) {
				//调用注册handler
				System.out.println("正在进行注册处理...");
				LoginHandle login = new LoginHandle(get,write,read);
				outtemp+=login.getLogin(input,temp[0]);
			}else if (isCommand(temp[0],signIn)) {
				//调用登录handler
				System.out.println("正在进行登录处理...");
				SignInHandle signin = new SignInHandle(get,read,write);
				outtemp+=signin.getSignIn(input,temp[0]);
			}else if (isCommand(temp[0],add)) {
				//调用添加handler
				System.out.println("正在进行添加处理...");
				AddHandle addHandle=new AddHandle(get,write,read);
				outtemp+=addHandle.Add(input, temp[0]);
			}else if (isCommand(temp[0],friendsList)) {
				//调用列表handler
				System.out.println("正在进行列表处理...");
				FriendsListHandle friendList = new FriendsListHandle(get,write,read);
				outtemp+=friendList.getList(input, temp[0]);
			}else if (isCommand(temp[0],downline)) {
				//调用downline handler
				System.out.println("正在进行下线处理...");
				DownLineHandle downLineHandle=new DownLineHandle(get,write,read);
				outtemp+=downLineHandle.getDownLine(input, temp[0]);
			}else if (isCommand(temp[0], mysise)) {
				//调用mysise handler
				System.out.println("正在进行mysise相关处理...");
				MysiseHandle mysiseHandle=new MysiseHandle(get,write,read);
				outtemp+=mysiseHandle.bandMysise(input, temp[0]);
			}else if (isCommand(temp[0], email)) {
				//调用邮箱handler
				System.out.println("正在进行邮箱相关处理...");
				EmailHandle emailHandle=new EmailHandle(get,write,read);
				outtemp+=emailHandle.bandEmail(input, temp[0]);
			}else if (isCommand(temp[0],information)) {
				//调用信息handler
				System.out.println("正在进行信息处理...");
				InformationHandle informationHandle=new InformationHandle(get,write,read);
				outtemp+=informationHandle.getInformation(input, temp[0]);
			}else if (isCommand(temp[0], file)) {
				//调用文件handler
			}else if (isCommand(temp[0], message)) {
				//调用获取离线缓存信息与文件
				System.out.println("正在进行离线信息处理...");
				GetMessage getMessage = new GetMessage(get,write,read);
				outtemp+=getMessage.get(input, temp[0]);
			}else if (isCommand(temp[0], publice)) {
				//调用公共模块
				System.out.println("正在进行公共模块处理...");
				PubliceGround publiceGround=new PubliceGround(get, write, read);
				outtemp+=publiceGround.publice(input, temp[0]);
			}else if (isCommand(temp[0], hiddenlove)) {
				//调用暗恋模块
				System.out.println("正在进行暗恋模块处理...");
				HiddenloveHandle HLH=new HiddenloveHandle(get, write, read);
				outtemp+=HLH.get(input, temp[0]);
			}else {
				System.out.println("指令错误，结束处理...");
				//返回指令错误
			}
		}
		byte[] out=outtemp.getBytes("UTF-8");
		return out;
	}
	
	/**
	 * <p>读取请求的长度，进行自动分包（私有<p>
	 * 
	 * @param dataInput
	 * @return str
	 */
	private List<String> changeString(String input, List<String> outputlist) {
		String temp = "";
		if (outputlist == null) {
			outputlist = new ArrayList<String>();
		}
		if (!(temp.isEmpty())) {
			input += temp;
		}
		String output = "";
		String pattern = "(?<=\\[length=)(\\d+)(?=\\])";
		int length;
		Pattern p = Pattern.compile(pattern);
		Matcher m = p.matcher(input);
		if (m.find()) {// 寻找匹配项
			// 获取消息字符串实际应有的长度
			length = Integer.parseInt(m.group(1));

			int startindex = input.indexOf("]") + 1;
			output = input.substring(startindex);

			if (output.length() == length) {
				outputlist.add(output);
				temp = "";
			} else if (output.length() < length) {
				temp = output;
			} else if (output.length() > length) {
				outputlist.add(output.substring(0, length));
				temp = "";
				input = input.substring(startindex + length);
				changeString(input, outputlist);
			}
		} else {
			temp = input;
		}

		return outputlist;
	}
	
	/**
	 * 读取动作指令
	 * 
	 * @param str
	 * @return strCommande
	 */
	private String[] getCommande(String str) {
		String[] strCommande = new String[2];
		String pattern = "(?<=<)(\\w+)(?=>)";
		Pattern p = Pattern.compile(pattern);
		Matcher m = p.matcher(str);
		if (m.find()) {// 寻找匹配项
			strCommande[0] = m.group();
			strCommande[1] = str.substring(m.end() + 1, str.length());
		}
		return strCommande;
	}
	
	/**
	 * 命令集匹配
	 * 
	 * @param str
	 * @param command
	 * @return ture or false
	 */
	private boolean isCommand(String str,String[] command){
		for (int i = 0; i < command.length; i++) {
			if(str.equals(command[i])||str==command[i]){
				return true;
			}
		}
		return false;
	}
	
	/**
	 * 读取具体属性
	 * 
	 * @param str
	 * @return strValue
	 */
	private List<String> getValue(String str) {
		List<String> strValue = new ArrayList<String>();
		String pattern = "(?<=<)(\\w+:)(?=>)|(?<=>)([^><]+)";
		Pattern p = Pattern.compile(pattern);
		Matcher m = p.matcher(str);
		while (m.find()) {
			// 寻找匹配项
			strValue.add(m.group());
		}
		return strValue;
	}
	

}

