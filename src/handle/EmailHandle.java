package handle;

import java.util.List;

import dBase.DBRead;
import dBase.DBWrite;
import exception.ConfigurationException;
import file.Get;
/**Email操作类
 * 
 * @author jancojie
 *
 */
public class EmailHandle {
	 Get get=null;
	 DBWrite write=null;
	 DBRead read=null;
	public EmailHandle(Get get, DBWrite write, DBRead read) {
		this.get = get;
		this.write = write;
		this.read = read;
	}
	
	/**接口方法
	 * 
	 * @param value
	 * @param command
	 * @return
	 * @throws Throwable
	 */
	public String bandEmail(List<String> value, String command) throws Throwable {
		String[] str=getCommand();
		String valueString[] = value.toArray(new String[] {});
		if(command.equals(str[0])||command==str[0]){
			//绑定emill;
			String temp=setEmail(valueString);
			temp="[length="+temp.length()+"]"+temp;
			return temp;
		}
		return null;
	 } 
	
	/**获取操作命令
	 * 
	 * @return
	 */
	private String[] getCommand() {
		try {
			String[] command=get.getEmail();
			return command;
		} catch (ConfigurationException e) {
				e.printStackTrace();
		}
		return null;
	}
	
	/**绑定email
	 * 
	 * @param input
	 * @return
	 */
	private String setEmail(String[] input) {
		boolean set=write.setBandEmail_update(input[1],input[3],input[5]);
		if(set){
			return "<bandmyemailok>";
		}
		else {
			return "<bandmyemailunok>";
		}
		
	}
}
