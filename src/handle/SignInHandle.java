package handle;

import java.net.InetAddress;
import java.util.List;

import link.UDPServer;
import dBase.DBRead;
import dBase.DBWrite;
import dbClass.SignIn;
import exception.ConfigurationException;
import file.Get;
/**登录操作类
 * 
 * @author jancojie
 *
 */
public class SignInHandle {
	Get get = null;
	DBRead read = null;
	SignIn signin = new SignIn();
	DBWrite write = null;

	public SignInHandle(Get get, DBRead read, DBWrite write) {
		this.get = get;
		this.read = read;
		this.write = write;
	}

	/**接口方法
	 * 
	 * @param value
	 * @param command
	 * @return
	 * @throws Throwable
	 */
	public String getSignIn(List<String> value, String command)
			throws Throwable {
		String[] str = getCommand();
		String valueString[] = value.toArray(new String[] {});
		if (command.equals(str[0]) || command == str[0]) {
			// 登录
			int report = setSignIn(valueString);
			if (report == 0) {
				String temp = "<signinresultsuccessful><username:>"
						+ getNickname(signin.getUserId());
				String outStr="<systemmassage><title:>SiseChat<massage:>"+
						getNickname(signin.getUserId())+" 欢迎您回来!<type:>1";
				outStr = "[length=" + outStr.length() + "]" + outStr;
				temp = "[length=" + temp.length() + "]" + temp;
				return temp+outStr;
			} else if (report == 1 || report == 2) {
				String temp = "<signinresultunsuccessful><type:>" + report;
				temp = "[length=" + temp.length() + "]" + temp;
				return temp;
			} else {
				return null;
			}
		}
		return null;
	}
	
	/**取得操作命令
	 * 
	 * @return
	 */
	private String[] getCommand() {
		try {
			String[] command = get.getSignIn();
			return command;
		} catch (ConfigurationException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**登录操作
	 * 
	 * @param value
	 * @return
	 * @throws Throwable
	 */
	private int setSignIn(String[] value) throws Throwable {
		String pwd="";
		for (int i = 0; i < value.length; i++) {
			if (value[i].equals("userid:")) {
				signin.setUserId(value[i + 1]);
			} else if (value[i].equals("password:")) {
				pwd=value[i + 1];
			} else if (value[i].equals("ip:")) {
				signin.setIp(value[i + 1]);
			} else if (value[i].equals("port:")) {
				signin.setPort(Integer.parseInt(value[i + 1]));
			}
		}
		if (isUserId(signin.getUserId())
				&& isPsw(pwd, signin.getUserId())) {
			if (isStatus(signin.getUserId())) {
				write.setSignIn(signin.getUserId(), signin.getIp(),
						signin.getPort());
				sendSignInToFriends(signin);
				sendSignInTogFriends(signin);
				return 0;
			} else {
				return 2;
			}

		} else {
			return 1;
		}

	}

	/**上线好友通知（好友）
	 * 
	 * @param signIn
	 * @throws Throwable
	 */
	private void sendSignInToFriends(SignIn signIn) throws Throwable {
		InetAddress address = null;
		UDPServer udpServer = null;
		String[] friends = read.getFriends(signIn.getUserId());
		for (int i = 0; i < friends.length; i += 5) {
			String fid = friends[i];
			if (isStatus_1(fid)) {
				String ip = signIn.getIp();
				String port = "" + signIn.getPort();
				String fip = friends[i + 4];
				address = InetAddress.getByName(fip);
				int fport = Integer.parseInt(friends[i + 1]);
				String out = "<youfriendsignin><userid:>" + signIn.getUserId()
						+ "<name:>" + read.getNickname(signIn.getUserId())
						+ "<ip:>" + ip + "<port:>" + port;
				out = "[length=" + out.length() + "]" + out;
				udpServer = new UDPServer(out, address, fport);
				udpServer.send();
			}
		}

	}

	/**上线好友通知（群）
	 * 
	 * @param signIn
	 * @throws Throwable
	 */
	private void sendSignInTogFriends(SignIn signIn) throws Throwable {
		InetAddress address = null;
		UDPServer udpServer = null;
		String name = read.getNickname(signIn.getUserId());
		String[] groud = read.getMYUserGroupList(signIn.getUserId());
		for (int i = 0; i < groud.length; i+=2) {
			String[] fid = read.getusergroupFriend_userId(groud[i]);
			for (int j = 0; j < fid.length; j++) {
				String[] fip = read.getIPPort(fid[j]);
				if(fid[j].equals(signIn.getUserId())){
					continue;
				}
				if (isStatus_1(fid[j])) {
					address = InetAddress.getByName(fip[0]);
					int fport = Integer.parseInt(fip[1]);
					String out = "<yougfriendsignin><talkroomid:>" + groud[i]
							+ "<userid:>" + signIn.getUserId() + "<name:>"
							+ name + "<ip:>" + signIn.getIp() + "<port>"
							+ signIn.getPort();
					out = "[length=" + out.length() + "]" + out;
					udpServer = new UDPServer(out, address, fport);
					udpServer.send();
				}
			}
		}

	}
	
	/**判断用户是否存在
	 * 
	 * @param userId
	 * @return
	 */
	private boolean isUserId(String userId) {
		String[] str = read.getUserInfoId();
		for (int i = 0; i < str.length; i++) {
			if (userId.equals(str[i]) || userId == str[i]) {
				return true;
			}
		}
		return false;
	}
	
	/**判断用户密码时候正确
	 * 
	 * @param psw
	 * @param userId
	 * @return
	 */
	private boolean isPsw(String psw, String userId) {
		String temp = read.getPwd(userId);
		if (psw.equals(temp) || psw == temp) {
			return true;
		}
		return false;

	}
	
	/**判断用户是否在线(登录)
	 * 
	 * @param userId
	 * @return 在线返回false
	 */
	private boolean isStatus(String userId) {
		int i = read.getStatus(userId);
		if (i == 1) {
			return false;
		} else {
			return true;
		}
	}
	
	/**判断用户是否在线(登陆后)
	 * 
	 * @param userId
	 * @return 在线返回true
	 */
	private boolean isStatus_1(String userId) {
		int i = read.getStatus(userId);
		if (i == 1) {
			return true;
		} else {
			return false;
		}
	}

	/**取得好友nickname
	 * 
	 * @param userId
	 * @return
	 */
	private String getNickname(String userId) {
		String nickname = read.getNickname(userId);
		return nickname;
	}

}
