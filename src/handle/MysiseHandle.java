package handle;

import java.net.InetAddress;
import java.util.List;

import link.UDPServer;
import dBase.DBRead;
import dBase.DBWrite;
import exception.ConfigurationException;
import file.Get;

/**mysise信息处理类
 * 
 * @author jancojie
 *
 */
public class MysiseHandle {
	Get get = null;
	DBWrite write = null;
	DBRead read = null;

	
	public MysiseHandle(Get get, DBWrite write, DBRead read) {
		this.get = get;
		this.write = write;
		this.read = read;
	}

	/**命令分发
	 * 
	 * @param value
	 * @param command
	 * @return
	 * @throws Throwable
	 */
	public String bandMysise(List<String> value, String command)
			throws Throwable {
		String[] str = getCommand();
		String valueString[] = value.toArray(new String[] {});
		if (command.equals(str[0]) || command == str[0]) {
			// mysise信息设置
			String temp = setMysise(valueString);

			return temp;
		}
		return null;
	}

	/**取得命令
	 * 
	 * @return
	 */
	private String[] getCommand() {
		try {
			String[] command = get.getMysise();
			return command;
		} catch (ConfigurationException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**mysise信息设置
	 * 
	 * @param input
	 * @return
	 * @throws Throwable
	 */
	private String setMysise(String[] input) throws Throwable {
		String out;
		String out_1;
		boolean set_1 = write.setmysise(input[1], input[3], input[5], input[7],
				input[9], input[11]);
		boolean set_2 = write.setEmail_update(input[1], input[13]);

		if (set_1 && set_2) {
			out_1 = autoSetUserGround(input[1], input[9]);
			out = "<bandmysiseok>";
		} else {
			out = "<bandmysiseunok>";
			out_1="";
		}

		out = "[length=" + out.length() + "]" + out;
		out_1 = "[length=" + out_1.length() + "]" + out_1;
		return out+out_1;
	}

	/**自动分组
	 * 
	 * @param userId
	 * @param mclass
	 * @return
	 * @throws Throwable
	 */
	private String autoSetUserGround(String userId, String mclass)
			throws Throwable {
		String output;
		if (read.isUserGrounpExist(mclass)) {
			output = "<logintalkroomresult><type:>1<talkroomid:>" + mclass
					+ "<talkroomname:>" + mclass;
			write.setUserGroup(userId, mclass, mclass);
			return output;
		} else {
			write.setUserGroupFriend(userId, mclass, mclass);
			// 发送除添加者以外的聊天室成员
			String[] guserId = read.getusergroupFriend_userId(mclass);
			for (int i = 0; i < guserId.length; i++) {
				String[] fip = read.getIPPort(guserId[i]);
				if (isStatus(guserId[i], read)) {
					String[] userIpPort = read.getIPPort(guserId[i]);
					String outStr_1 = "<addtalkroomfriendback2><fromid:>"
							+ userId + "<fromname:>" + read.getNickname(userId)
							+ "<ip:>" + fip[0] + "<port:>" + fip[1]
							+ "<talkroomid:>" + mclass;
					InetAddress address_1 = null;
					address_1 = InetAddress.getByName(userIpPort[0]);
					UDPServer UDP = new UDPServer(outStr_1, address_1,
							Integer.parseInt(userIpPort[1]));
					UDP.send();
				}
			}
			output = "<classtalkroom><talkroomid:>"+mclass+"<talkroomname:>"+mclass;
			return output;
		}

	}

	/**判断用户是否在线
	 * 
	 * @param userId
	 * @param read
	 * @return
	 */
	private boolean isStatus(String userId, DBRead read) {
		int i = read.getStatus(userId);
		if (i == 1) {
			return true;
		} else {
			return false;
		}
	}
}
