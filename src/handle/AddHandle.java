package handle;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;

import link.UDPServer;
import dBase.DBRead;
import dBase.DBWrite;
import exception.ConfigurationException;
import file.Get;

public class AddHandle {
	Get get = null;
	DBWrite write = null;
	DBRead read = null;

	public AddHandle(Get get, DBWrite write, DBRead read) {
		this.get = get;
		this.write = write;
		this.read = read;
	}

	public String Add(List<String> value, String command) throws Throwable {
		String[] str = getCommand();
		String valueString[] = value.toArray(new String[] {});
		if (command.equals(str[0]) || command == str[0]) {
			// 添加好友；
			String temp = sendToFriends(valueString);
			temp = "[length=" + temp.length() + "]" + temp;
			return temp;

		} else if (command.equals(str[1]) || command == str[1]) {
			// 添加好友返回；
			String temp = addUserFriendBack(valueString);
			return temp;
		} else if (command.equals(str[2]) || command == str[2]) {
			// 发送所有用户列表；
			String temp[] = getAddList(valueString);
			String temp_1 = "";
			for (int i = 0; i < temp.length; i++) {
				temp_1 += temp[i];
			}
			temp_1 = "<setaddlist>" + temp_1;
			temp_1 = "[length=" + temp_1.length() + "]" + temp_1;
			return temp_1;
		} else if (command.equals(str[3]) || command == str[3]) {
			// 发送聊天室列表；
			String temp[] = getAddTRList(valueString);
			String temp_1 = "";
			for (int i = 0; i < temp.length; i++) {
				temp_1 += temp[i];
			}
			temp_1 = "<findtalkroomresult>" + temp_1;
			temp_1 = "[length=" + temp_1.length() + "]" + temp_1;
			return temp_1;
		} else if (command.equals(str[4]) || command == str[4]) {
			// 聊天室添加成员；
			String temp = sendToTRFriends(valueString);
			temp = "[length=" + temp.length() + "]" + temp;
			return temp;
		} else if (command.equals(str[5]) || command == str[5]) {
			// 聊天室添加成员缓存命令
			String temp = addUsergFriendBack(valueString);
			temp = "[length=" + temp.length() + "]" + temp;
			return temp;
		} else if(command.equals(str[6]) || command == str[6]){
			//申请加入聊天室
			String temp=addtalkroominvite(valueString);
			temp = "[length=" + temp.length() + "]" + temp;
			return temp;
		}else if(command.equals(str[7]) || command == str[7]){
			//申请加入聊天室返回
			String temp=addtalkroominviteback(valueString);
			temp = "[length=" + temp.length() + "]" + temp;
			return temp;
		}
		return null;

	}
	private String addtalkroominviteback(String[] valueString) throws Throwable {
		String fuserId = valueString[1];
		String gId = valueString[3];
		String gname = valueString[5];
		String userId =valueString[7];//群主id
		String type = valueString[9];
		String fname =read.getNickname(fuserId);
		UDPServer UDP = null;
		String[] ip = read.getIPPort(fuserId);
		String outStr = "";
		InetAddress address = null;
		address = InetAddress.getByName(ip[0]);
		if (isStatus(fuserId, read)) {
			if (type.equals("1") || type == "1") {
				write.setUserGroupFriend(fuserId, gId, gname);
				outStr = "<addtalkroominviteback><talkroomid:>" + gId
						+ "<talkroomname:>" + gname + 
						"<fromuserid:>"+userId+"<type:>1";
				outStr = "[length=" + outStr.length() + "]" + outStr;
				UDP = new UDPServer(outStr, address, Integer.parseInt(ip[1]));
				UDP.send();
				String[] guserId = read.getusergroupFriend_userId(gId);
				// 发送除添加者以外的聊天室成员
				for (int i = 0; i < guserId.length; i++) {
					if (isStatus(guserId[i], read)) {
						String[] userIpPort = read.getIPPort(guserId[i]);
						String outStr_1 = "<addtalkroomfriendback2><fromid:>"
								+ fuserId
								+ "<fromname:>"
								+ fname
								+ "<ip:>"
								+ ip[0]
								+ "<port:>"
								+ ip[1]
								+ "<talkroomid:>" + gId;
						InetAddress address_1 = null;
						address_1 = InetAddress.getByName(userIpPort[0]);
						UDP = new UDPServer(outStr_1, address_1,
								Integer.parseInt(userIpPort[1]));
						UDP.send();
					}
				}
			} else {
				outStr ="<addtalkroominviteback><talkroomid:>" + gId
						+ "<talkroomname:>" + gname + 
						"<fromuserid:>"+userId+"<type:>0";
				outStr = "[length=" + outStr.length() + "]" + outStr;
				UDP = new UDPServer(outStr, address, Integer.parseInt(ip[1]));
				UDP.send();
			}

		} else {
			if(type.equals("1")||type=="1"){
				write.setUserGroupFriend(fuserId, gId, gname);
			}
			write.setBackgFriendCache_add(fuserId, userId, type, gId, gname,"1");;
		}
		return null;
	}

	/**用户申请加入群
	 * 
	 * @param valueString
	 * @return
	 * @throws Throwable
	 */
private String addtalkroominvite(String[] valueString) throws Throwable {
	String fuserId = valueString[1];
	String gId = valueString[3];
	String[] userId = read.getusergroup_findUserId(gId);//群主
	String gname=read.getusergroup_findgname(gId);
	String fip[]=read.getIPPort(fuserId);
	String fname=read.getNickname(fuserId);
	UDPServer UDP = null;
	String outStr = "<addtalkroominvite><userid:>" + fuserId
			+ "<name:>"+fname+"<talkroomid:>" + gId + "<ip:>"+fip[0]+
			"<port:>"+fip[1];
	outStr = "[length=" + outStr.length() + "]" + outStr;
	String output = "";
		if (isStatus(userId[0], read)) {
			InetAddress address = null;
			String ip[] =read.getIPPort(userId[0]);
			address = InetAddress.getByName(fip[0]);
			UDP = new UDPServer(outStr, address, Integer.parseInt(ip[1]));
			String[] inStr = UDP.server();
			if (inStr[9].equals("1") || inStr[9] == "1") {
				String[] guserId = read.getusergroupFriend_userId(gId);
				write.setUserGroupFriend(fuserId, gId, gname);
				output = "<addtalkroominviteback><talkroomid:>" + gId
						+ "<talkroomname:>" + gname + 
						"<fromuserid:>"+userId[0]+"<type:>1";
				// 发送除添加者以外的聊天室成员
				for (int i = 0; i < guserId.length; i++) {
					if (isStatus(guserId[i], read)) {
						String[] userIpPort = read.getIPPort(guserId[i]);
						String outStr_1 = "<addtalkroomfriendback2><fromid:>"
								+ fuserId
								+ "<fromname:>"
								+ fname
								+ "<ip:>"
								+ fip[0]
								+ "<port:>"
								+ fip[1]
								+ "<talkroomid:>" + gId;
						InetAddress address_1 = null;
						address_1 = InetAddress.getByName(userIpPort[0]);
						UDP = new UDPServer(outStr_1, address_1,
								Integer.parseInt(userIpPort[1]));
						UDP.send();
					}
				}
			} else {
				output = "<addtalkroominviteback><talkroomid:>" + gId
						+ "<talkroomname:>" + gname + 
						"<fromuserid:>"+userId[0]+"<type:>0";
			}

		} else {
			write.setGetgFriends_add(fuserId, userId[0], gId, gname,"1");
		}
	return output;
	}

/**添加聊天室成员返回
 * 
 * @param valueString
 * @return
 * @throws Throwable
 */
	private String addUsergFriendBack(String[] valueString) throws Throwable {
		String userId = valueString[1];
		String fuserId = valueString[3];
		String type = valueString[5];
		String gId = valueString[7];
		String fname = read.getNickname(fuserId);
		String[] gname = read.getusergroup_findGid(gId);
		UDPServer UDP = null;
		String[] fip = read.getIPPort(fuserId);
		String[] ip = read.getIPPort(userId);
		String name = read.getNickname(userId);
		String outStr = "";
		String output = "";
		InetAddress address = null;
		address = InetAddress.getByName(ip[0]);
		if (isStatus(userId, read)) {
			if (type.equals("1") || type == "1") {
				write.setUserGroupFriend(fuserId, gId, gname[1]);
				outStr = "<addtalkroomfriendback><fromid:>" + fuserId
						+ "<fromname:>" + fname + "<type:>1<ip:>" + fip[0]
						+ "<port:>" + fip[1] + "<talkroomid:>" + gId;
				outStr = "[length=" + outStr.length() + "]" + outStr;
				UDP = new UDPServer(outStr, address, Integer.parseInt(ip[1]));
				UDP.send();
				String[] guserId = read.getusergroupFriend_userId(gId);
				// 发送除添加者以外的聊天室成员
				for (int i = 0; i < guserId.length; i++) {
					if (isStatus(guserId[i], read)) {
						String[] userIpPort = read.getIPPort(guserId[i]);
						String outStr_1 = "<addtalkroomfriendback2><fromid:>"
								+ fuserId + "<fromname:>" + fname + "<ip:>"
								+ fip[0] + "<port:>" + fip[1] + "<talkroomid:>"
								+ gId;
						InetAddress address_1 = null;
						address_1 = InetAddress.getByName(userIpPort[0]);
						UDP = new UDPServer(outStr_1, address_1,
								Integer.parseInt(userIpPort[1]));
						UDP.send();
					}
				}
				output = "<TRok><userId:>" + userId + "<name:>" + name;
			} else {
				outStr = "<addtalkroomfriendback><fromid:>" + fuserId
						+ "<fromname:>" + fname + "<type:>0<talkroomid:>" + gId;
				outStr = "[length=" + outStr.length() + "]" + outStr;
				UDP = new UDPServer(outStr, address, Integer.parseInt(ip[1]));
				UDP.send();
				output = "<TRok><userId:>" + userId + "<name:>" + name;
			}

		} else {
			if(type.equals("1")||type=="1"){
				write.setUserGroupFriend(fuserId, gId, gname[1]);
			}
			write.setBackgFriendCache(userId, fuserId, type, gId, gname[1]);;
			output = "<TRunok><userId:>" + userId + "<name:>" + name;
		}
		return output;
	}
/**添加聊天室成员
 * 
 * @param valueString
 * @return
 * @throws Throwable
 */
	private String sendToTRFriends(String[] valueString) throws Throwable {
		String fuserId = valueString[1];
		String userId = valueString[3];
		String gId = valueString[5];
		String gname = valueString[7];
		UDPServer UDP = null;
		String[] fip = read.getIPPort(fuserId);
		String fname = read.getNickname(fuserId);
		String outStr = "<addtalkroomfriendyou><fromid:>" + userId
				+ "<fromname:>" + read.getNickname(userId) + "<talkroomid:>"
				+ gId + "<talkroomname:>" + gname;
		outStr = "[length=" + outStr.length() + "]" + outStr;
		String output = "";
		if (read.isUserGrounpFriend(fuserId, gId)) {
			if (isStatus(fuserId, read)) {
				InetAddress address = null;
				address = InetAddress.getByName(fip[0]);
				UDP = new UDPServer(outStr, address, Integer.parseInt(fip[1]));
				String[] inStr = UDP.server();
				String[] guserId = read.getusergroupFriend_userId(gId);
				if (inStr[5].equals("1") || inStr[5] == "1") {
					write.setUserGroupFriend(fuserId, gId, gname);
					output = "<addtalkroomfriendback><fromid:>" + fuserId
							+ "<fromname:>" + fname + "<type:>1<ip:>" + fip[0]
							+ "<port:>" + fip[1] + "<talkroomid:>" + gId;
					// 发送除添加者以外的聊天室成员
					for (int i = 0; i < guserId.length; i++) {
						if (isStatus(guserId[i], read)) {
							String[] userIpPort = read.getIPPort(guserId[i]);
							String outStr_1 = "<addtalkroomfriendback2><fromid:>"
									+ fuserId
									+ "<fromname:>"
									+ fname
									+ "<ip:>"
									+ fip[0]
									+ "<port:>"
									+ fip[1]
									+ "<talkroomid:>" + gId;
							InetAddress address_1 = null;
							address_1 = InetAddress.getByName(userIpPort[0]);
							UDP = new UDPServer(outStr_1, address_1,
									Integer.parseInt(userIpPort[1]));
							UDP.send();
						}
					}
				} else {
					output = "<addtalkroomfriendback><fromid:>" + fuserId
							+ "<fromname:>" + fname + "<type:>0<talkroomid:>"
							+ gId;
				}

			} else {
				write.setGetgFriends(userId, fuserId, gId, gname);
				output = "<TRunok><userId:>" + fuserId + "<name:>" + fname+"<talkroomid:>"+gId+"<talkroomname:>" + gname;
			}
		} else {
			output="<trfrienderror><userId:>"+fuserId+"<name:>"+fname+"<talkroomid:>"+gId+"<talkroomname:>" + gname;
		}
		return output;
	}
/**获取所有聊天室列表
 * 
 * @param valueString
 * @return
 */
	private String[] getAddTRList(String[] valueString) {
		String gId = valueString[1];
		String gname = valueString[3];
		String[] input;
		List<String> output = new ArrayList<String>();
		if ((gId.equals("_empty_") || gId == "_empty_")
				&& !(gname.equals("_empty_") || gname == "_empty_")) {
			input = read.getusergroup_findGname(gname);
			for (int i = 0; i < input.length; i += 2) {
				String id = input[i];
				String name = input[i + 1];
				String str = "<talkroomid:>" + id + "<talkroomname:>" + name;
				output.add(str);
			}
		} else if (!(gId.equals("_empty_") || gId == "_empty_")
				&& (gname.equals("_empty_") || gname == "_empty_")) {
			input = read.getusergroup_findGid(gId);
			for (int i = 0; i < input.length; i += 2) {
				String id = input[i];
				String name = input[i + 1];
				String str = "<talkroomid:>" + id + "<talkroomname:>" + name;
				output.add(str);
			}
		} else if (!(gId.equals("_empty_") || gId == "_empty_")
				&& !(gname.equals("_empty_") || gname == "_empty_")) {
			input = read.getusergroup_findGidGname(gname, gId);
			for (int i = 0; i < input.length; i += 2) {
				String id = input[i];
				String name = input[i + 1];
				String str = "<talkroomid:>" + id + "<talkroomname:>" + name;
				output.add(str);
			}
		}else {
			input = read.getusergroup();
			for (int i = 0; i < input.length; i += 2) {
				String id = input[i];
				String name = input[i + 1];
				String str = "<talkroomid:>" + id + "<talkroomname:>" + name;
				output.add(str);
		  }
		}
		String str[] = output.toArray(new String[] {});
		return str;
	}
/**取的命令
 * 
 * @return
 */
	private String[] getCommand() {
		try {
			String[] command = get.getAdd();
			return command;
		} catch (ConfigurationException e) {
			e.printStackTrace();
		}
		return null;
	}
/**添加好友
 * 
 * @param valueString
 * @return
 * @throws Throwable
 */
	private String sendToFriends(String[] valueString) throws Throwable {
		String userId = valueString[3];
		String fuserId = valueString[1];
		UDPServer UDP = null;
		String[] fip = read.getIPPort(fuserId);
		String[] ip = read.getIPPort(userId);
		String fname = read.getNickname(fuserId);
		String name = read.getNickname(userId);
		String outStr = "<addyou><fromuserid:>" + userId + "<fromname:>" + name
				+ "<ip:>" + ip[0] + "<port:>" + ip[1];
		outStr = "[length=" + outStr.length() + "]" + outStr;
		String output = "";
		if(read.isFriends(userId, fuserId)){
			if (isStatus(fuserId, read)) {
				InetAddress address = null;
				address = InetAddress.getByName(fip[0]);
				UDP = new UDPServer(outStr, address, Integer.parseInt(fip[1]));
				String[] inStr = UDP.server();
				if (inStr[5].equals("1") || inStr[5] == "1") {
					write.setFriends(userId, fuserId, fname, fip[0], fip[1], ip[0],
							fip[1], name);
					output = "<adduserfriendback><fromname:>" + fname
							+ "<fromuserid:>" + fuserId + "<type:>1<ip:>" + fip[0]
							+ "<port:>" + fip[1];
				} else {
					output = "<adduserfriendback><fromname:>" + fname
							+ "<fromuserid:>" + fuserId + "<type:>0";
				}
	
			} else {
				if(read.isgetGetFriend(fuserId, userId)){
					write.setGetFriends(userId, name, fuserId, fname);
				}
				output = "<unok><userId:>" + fuserId + "<name:>" + fname;
			}
		}
		return output;
	}

	/**获取添加好友返回
	 * 
	 * @param valueString
	 * @return
	 * @throws Throwable
	 */
	private String addUserFriendBack(String[] valueString) throws Throwable {
		String userId = valueString[1];
		String fuserId = valueString[3];
		String type = valueString[5];
		UDPServer UDP = null;
		String[] fip = read.getIPPort(fuserId);
		String[] ip = read.getIPPort(userId);
		String name = read.getNickname(userId);
		String fname = read.getNickname(fuserId);
		String outStr = "";
		InetAddress address = null;
		address = InetAddress.getByName(ip[0]);
		if (isStatus(userId, read)) {
			if (type.equals("1") || type == "1") {
				write.setFriends(userId, fuserId, fname, fip[0], fip[1], ip[0],
						ip[1], name);
				outStr = "<adduserfriendback>"
						+ "<fromname:>" + fname + "<fromuserid:>" + fuserId
						+ "<type:>1<ip:>" + fip[0] + "<port:>" + fip[1];
				outStr = "[length=" + outStr.length() + "]" + outStr;
				UDP = new UDPServer(outStr, address, Integer.parseInt(ip[1]));
				UDP.send();
			} else {
				outStr = "<adduserfriendback>"
						+ "<fromname:>" + fname + "<fromuserid:>" + fuserId
						+ "<type:>0";
				outStr = "[length=" + outStr.length() + "]" + outStr;
				UDP = new UDPServer(outStr, address, Integer.parseInt(ip[1]));
				UDP.send();
			}
		} else {
			write.setBackFriendCache(userId, fuserId, type);
		}
		return null;
	}

	/**获取所有用户
	 * 
	 * @param valueString
	 * @return
	 */
	private String[] getAddList(String[] valueString) {
		String[] userId = read.getUserInfoId();
		List<String> output = new ArrayList<String>();
		for (int i = 0; i < userId.length; i++) {
			if(userId[i].equals(valueString[1])||userId[i]==valueString[1]){
				continue;
			}
			String userInfo[] = read.getUserlnfo_nick_sex(userId[i]);
			String mysise[] = read.getMysise_major_department(userId[i]);
			String addinfo = "<userid:>" + userId[i] + "<name:>" + userInfo[0]
					+ "<sex:>" + userInfo[1] + "<department:>" + mysise[1]
					+ "<major:>" + mysise[0];
			output.add(addinfo);
		}
		String str[] = output.toArray(new String[] {});
		return str;

	}

	private boolean isStatus(String userId, DBRead read) {
		int i = read.getStatus(userId);
		if (i == 1) {
			return true;
		} else {
			return false;
		}
	}

}
