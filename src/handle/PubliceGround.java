package handle;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;

import link.UDPServer;
import dBase.DBRead;
import dBase.DBWrite;
import exception.ConfigurationException;
import file.Get;

/**
 * 公共板块处理类
 * 
 * @author jancojie
 * 
 */
public class PubliceGround {
	Get get = null;
	DBWrite write = null;
	DBRead read = null;

	public PubliceGround(Get get, DBWrite write, DBRead read) {
		this.get = get;
		this.write = write;
		this.read = read;
	}

	/**
	 * TODO 公共板块对外接口
	 * 
	 * @param value
	 * @param command
	 * @return
	 * @throws Throwable
	 */
	public String publice(List<String> value, String command) throws Throwable {
		String[] str = getCommand();
		String valueString[] = value.toArray(new String[] {});
		String temp = "";
		if (command.equals(str[0]) || command == str[0]) {
			// 添加失物；
			temp = addlostthing(valueString);
			temp = "[length=" + temp.length() + "]" + temp;
		} else if (command.equals(str[1]) || command == str[1]) {
			// 删除失物
			temp = deletelostthing(valueString);
			temp = "[length=" + temp.length() + "]" + temp;
		} else if (command.equals(str[2]) || command == str[2]) {
			// 添加拾获
			temp = addfoundthing(valueString);
			temp = "[length=" + temp.length() + "]" + temp;
		} else if (command.equals(str[3]) || command == str[3]) {
			// 删除拾获
			temp = deletefoundthing(valueString);
			temp = "[length=" + temp.length() + "]" + temp;
		} else if (command.equals(str[4]) || command == str[4]) {
			// 获取树洞帖子
			temp = addalltrees(valueString);
			temp = "[length=" + temp.length() + "]" + temp;
		} else if (command.equals(str[5]) || command == str[5]) {
			// 创建新树洞
			temp = addtrees(valueString);
			temp = "[length=" + temp.length() + "]" + temp;
		} else if (command.equals(str[6]) || command == str[6]) {
			// 回复树洞
			temp = addtreesreply(valueString);
			temp = "[length=" + temp.length() + "]" + temp;
		} else if (command.equals(str[7]) || command == str[7]) {
			// 获取所有失物
			temp = addAllLostThing(valueString);
			temp = "[length=" + temp.length() + "]" + temp;
		} else if (command.equals(str[8]) || command == str[8]) {
			// 获取所有拾物
			temp = addAllFoundThing(valueString);
			temp = "[length=" + temp.length() + "]" + temp;
		} else if (command.equals(str[9]) || command == str[9]) {
			// 删除树帖
			temp = deleteTrees(valueString);
			temp = "[length=" + temp.length() + "]" + temp;
		} else if (command.equals(str[10]) || command == str[10]) {
			// 删除回复
			temp = deleteTreesReply(valueString);
			temp = "[length=" + temp.length() + "]" + temp;
		} else {
			temp = null;
		}
		return temp;
	}

	/**
	 * TODO 获取所有拾获
	 * 
	 * @param valueString
	 * @return
	 */
	private String addAllFoundThing(String[] valueString) {
		String[] foundthing = read.getfoundthing();
		List<String> output = new ArrayList<String>();
		for (int i = 0; i < foundthing.length; i += 9) {
			String string = "<title:>" + foundthing[i + 1] + "<type:>"
					+ foundthing[i + 2] + "<cardnumber:>" + foundthing[i + 3]
					+ "<cardname:>" + foundthing[i + 4] + "<describption:>"
					+ foundthing[i + 5] + "<time:>" + foundthing[i + 6]
					+ "<telephone:>" + foundthing[i + 7] + "<userid:>"
					+ foundthing[i + 8] + "<id:>" + foundthing[i];
			output.add(string);
		}
		String str[] = output.toArray(new String[] {});
		String out = "<getallfoundthing>";
		for (int i = 0; i < str.length; i++) {
			out += str[i];
		}
		return out;
	}

	/**
	 * TODO 获取所有失物
	 * 
	 * @param valueString
	 * @return string
	 */
	private String addAllLostThing(String[] valueString) {
		String[] lostthing = read.getlostthing();
		List<String> output = new ArrayList<String>();
		for (int i = 0; i < lostthing.length; i += 11) {
			String string = "<title:>" + lostthing[i + 1] + "<type:>"
					+ lostthing[i + 2] + "<cardnumber:>" + lostthing[i + 3]
					+ "<cardname:>" + lostthing[i + 4] + "<describption:>"
					+ lostthing[i + 5] + "<time:>" + lostthing[i + 6]
					+ "<realname:>" + lostthing[i + 7] + "<department:>"
					+ lostthing[i + 8] + "<telephone:>" + lostthing[i + 9]
					+ "<userid:>" + lostthing[i + 10] + "<id:>" + lostthing[i];
			output.add(string);
		}
		String str[] = output.toArray(new String[] {});
		String out = "<getalllostthing>";
		for (int i = 0; i < str.length; i++) {
			out += str[i];
		}
		return out;
	}

	/**
	 * TODO 删除回复
	 * 
	 * @param valueString
	 * @return string
	 */
	private String deleteTreesReply(String[] valueString) {
		if (write.deletetreesreply(valueString[1])) {
			return "<treebackdeletesuccessful><replyno:>" + valueString[1];
		} else {
			return "<treebackdeleteunsuccessful><replyno:>" + valueString[1];
		}
	}

	/**
	 * TODO 删除树贴
	 * 
	 * @param valueString
	 * @return
	 */
	private String deleteTrees(String[] valueString) {
		if (write.deletetrees(valueString[1])) {
			return "<treeseletesuccessful><no:>" + valueString[1];
		} else {
			return "<treeseleteunsuccessful><no:>" + valueString[1];
		}
	}

	/**
	 * TODO 获得所有树洞信息
	 * 
	 * @param valueString
	 * @return
	 */
	private String addalltrees(String[] valueString) {
		String[] trees = read.gettress();
		List<String> output = new ArrayList<String>();
		for (int i = 0; i < trees.length; i += 6) {
			if (trees[i + 4].equals("1")) {
				String[] treesreply = read.gettressreply(trees[i]);
				String reply = "";
				String temp = "<no:>" + trees[i] + "<massage:>" + trees[i + 1]
						+ "<date:>" + trees[i + 2] + "<ower:>" + trees[i + 3]
						+ "<nametype:>" + trees[i + 5] + "<type:>"
						+ trees[i + 4] + "<count:>"
						+ treesreply[treesreply.length - 1];
				for (int j = 0; j < treesreply.length - 1; j += 5) {
					reply += "<replyno:>" + treesreply[j] + "<replyID:>"
							+ treesreply[j + 1] + "<nametype:>"
							+ treesreply[j + 2] + "<reply:>"
							+ treesreply[j + 3] + "<replydate:>"
							+ treesreply[j + 4];
				}
				temp += reply;
				output.add(temp);
			} else {
				String temp = "<no:>" + trees[i] + "<massage:>" + trees[i + 1]
						+ "<date:>" + trees[i + 2] + "<ower:>" + trees[i + 3]
						+ "<nametype:>" + trees[i + 5] + "<type:>"
						+ trees[i + 4] + "<count:>0";
				output.add(temp);
			}
		}
		String str[] = output.toArray(new String[] {});
		String out = "<gettrees>";
		for (int i = 0; i < str.length; i++) {
			out += str[i];
		}
		return out;
	}

	/**
	 * TODO 树洞回复
	 * 
	 * @param valueString
	 * @return string
	 * @throws Throwable
	 */
	private String addtreesreply(String[] valueString) throws Throwable {
		String owerid = read.gettressower(valueString[9]);
		if (read.isTress(valueString[9])) {
			String id = write.settreesreply(valueString[1], valueString[3],
					valueString[5], valueString[7], valueString[9]);
			String str = "<setreply><replyno:>" + id + "<replyID:>"
					+ valueString[1] + "<nametype:>" + valueString[3]
					+ "<reply:>" + valueString[5] + "<replydate:>"
					+ valueString[7] + "<no:>" + valueString[9];
			String outower = "<setreply><replyno:>" + id + "<replyID:>"
					+ valueString[1] + "<nametype:>" + valueString[3]
					+ "<reply:>" + valueString[5] + "<replydate:>"
					+ valueString[7] + "<no:>" + valueString[9];
			outower = "[length=" + outower.length() + "]" + outower;
			String[] ip = read.getIPPort(owerid);
			InetAddress address = InetAddress.getByName(ip[0]);
			UDPServer udpServer = new UDPServer(outower, address,
					Integer.parseInt(ip[1]));
			udpServer.send();
			return str;
		} else {
			return null;
		}

	}

	/**
	 * TODO 新建树洞
	 * 
	 * @param valueString
	 * @return string
	 * @throws Throwable
	 */
	private String addtrees(String[] valueString) throws Throwable {
		// String userId=valueString[7];
		String id = write.settrees(valueString[1], valueString[3],
				valueString[5], valueString[7]);
		String str = "<settress><massage:>" + valueString[1] + "<date:>"
				+ valueString[3] + "<ower:>" + valueString[5] + "<nametype:>"
				+ valueString[7] + "<id:>" + id;
		return str;
	}

	/**
	 * TODO 删除拾物
	 * 
	 * @param valueString
	 * @return
	 */
	private String deletefoundthing(String[] valueString) {
		String id = valueString[3];
		if (write.deletefoundthing(id)) {
			return "<founddeletesuccessful><id:>" + id;
		} else {
			return "<founddeleteunsuccessful><id:>" + id;
		}
	}

	/**
	 * TODO 添加拾物
	 * 
	 * @param valueString
	 * @return string
	 * @throws Throwable
	 */
	private String addfoundthing(String[] valueString) throws Throwable {

		String id = write.setfoundthing(valueString[1], valueString[3],
				valueString[5], valueString[7], valueString[9],
				valueString[11], valueString[13], valueString[15]);
		String str = "<addfoundthingsuccessful><title:>" + valueString[1]
				+ "<type:>" + valueString[3] + "<cardnumber:>" + valueString[5]
				+ "<cardname:>" + valueString[7] + "<describption:>"
				+ valueString[9] + "<time:>" + valueString[11] + "<telephone:>"
				+ valueString[13] + "<userid:>" + valueString[15] + "<id:>"
				+ id;
		String outstr = "<setfoundthing><title:>" + valueString[1] + "<type:>"
				+ valueString[3] + "<cardnumber:>" + valueString[5]
				+ "<cardname:>" + valueString[7] + "<describption:>"
				+ valueString[9] + "<time:>" + valueString[11] + "<telephone:>"
				+ valueString[13] + "<userid:>" + valueString[15] + "<id:>"
				+ id;
		outstr = "[length=" + outstr.length() + "]" + outstr;
		String[] ip = read.getstatusIpPort();
		for (int i = 0; i < ip.length; i += 3) {
			if (ip[i].equals(valueString[15])) {
				continue;
			}
			InetAddress address = InetAddress.getByName(ip[i + 1]);
			UDPServer udpServer = new UDPServer(outstr, address,
					Integer.parseInt(ip[i + 2]));
			udpServer.send();
		}
		// 判断拾物是否是饭卡
		if (valueString[3].equals("0")) {
			// 查找数据库中是否存在学号记录
			String[] siseno = read.getMysise_name(valueString[5]);
			for (int i = 0; i < siseno.length; i += 2) {
				if (!(siseno[i].equals("_empty_"))) {
					// 把饭卡拾获信息发送给相应用户
					if (isStatus(siseno[i + 1], read)) {
						String[] userip = read.getIPPort(siseno[i + 1]);
						InetAddress address = InetAddress.getByName(userip[0]);
						String outString = "<systemmassage><title:>有您的失物<massage:>"
								+ id + "<type:>0";
						outString = "[length=" + outString.length() + "]"
								+ outString;
						UDPServer udp = new UDPServer(outString, address,
								Integer.parseInt(userip[1]));
						udp.send();
					} else {
						// 用户不在线则进行数据库缓存
						write.setSystemMassage(siseno[i + 1], "有您的失物", id, "0");
					}
				}
			}
			// 查找数据库中是否存在相应寻物信息
			String[] lostthing = read.getlostthing();
			for (int i = 0; i < lostthing.length; i += 11) {
				if (lostthing[i + 3].equals(valueString[5])
						|| lostthing[i + 4].equals(valueString[7])) {
					if (isStatus(lostthing[i + 10], read)) {
						String[] userIp = read.getIPPort(lostthing[i + 10]);
						InetAddress address = InetAddress.getByName(userIp[0]);
						String found = "<setfoundthing><title:>"
								+ valueString[1] + "<type:>" + valueString[3]
								+ "<cardnumber:>" + valueString[5]
								+ "<cardname:>" + valueString[7]
								+ "<describption:>" + valueString[9]
								+ "<time:>" + valueString[11] + "<telephone:>"
								+ valueString[13] + "<userid:>"
								+ valueString[15] + "<id:>" + id;
						found = "[length=" + found.length() + "]" + found;
						String outStr = "<systemmassage><title:>有您的失物<massage:>"
								+ id + "<type:>0";
						outStr = "[length=" + outStr.length() + "]" + outStr;
						UDPServer udp = new UDPServer(found+outStr, address,
								Integer.parseInt(userIp[1]));
						udp.send();
					} else {
						// 用户不在线则进行数据库缓存
						write.setSystemMassage(lostthing[i + 10], "有您的失物", id,
								"0");
					}
				}
			}
		}
		return str;
	}

	/**
	 * TODO 删除失物
	 * 
	 * @param valueString
	 * @return string
	 */
	private String deletelostthing(String[] valueString) {
		String id = valueString[3];
		if (write.deletelostthing(id)) {
			return "<lostdeletesuccessful><id:>" + id;
		} else {
			return "<lostdeleteunsuccessful><id:>" + id;
		}

	}

	/**
	 * TODO 添加失物
	 * 
	 * @param valueString
	 * @return string
	 * @throws Throwable
	 */
	private String addlostthing(String[] valueString) throws Throwable {
		String id = write.setlostthing(valueString[1], valueString[3],
				valueString[5], valueString[7], valueString[9],
				valueString[11], valueString[13], valueString[15],
				valueString[17], valueString[19]);
		String str = "<addlostthingsuccessful><title:>" + valueString[1]
				+ "<type:>" + valueString[3] + "<cardnumber:>" + valueString[5]
				+ "<cardname:>" + valueString[7] + "<describption:>"
				+ valueString[9] + "<time:>" + valueString[11] + "<realname:>"
				+ valueString[13] + "<department:>" + valueString[15]
				+ "<telephone:>" + valueString[17] + "<userid:>"
				+ valueString[19] + "<id:>" + id;
		String outstr = "<setlostthing><title:>" + valueString[1] + "<type:>"
				+ valueString[3] + "<cardnumber:>" + valueString[5]
				+ "<cardname:>" + valueString[7] + "<describption:>"
				+ valueString[9] + "<time:>" + valueString[11] + "<realname:>"
				+ valueString[13] + "<department:>" + valueString[15]
				+ "<telephone:>" + valueString[17] + "<userid:>"
				+ valueString[19] + "<id:>" + id;
		outstr = "[length=" + outstr.length() + "]" + outstr;
		String[] ip = read.getstatusIpPort();
		for (int i = 0; i < ip.length; i += 3) {
			if (ip[i].equals(valueString[19])) {
				continue;
			}
			InetAddress address = InetAddress.getByName(ip[i + 1]);
			UDPServer udpServer = new UDPServer(outstr, address,
					Integer.parseInt(ip[i + 2]));
			udpServer.send();
		}
		// 查找数据库中是否存在相应寻物信息
		if (valueString[3].equals("0")) {
			String[] founding = read.getfoundthing();
			for (int i = 0; i < founding.length; i += 9) {
				if (founding[i + 3].equals(valueString[5])
						|| founding[i + 4].equals(valueString[7])) {
					String[] userIp = read.getIPPort(valueString[19]);
					InetAddress address = InetAddress.getByName(userIp[0]);
					String found = "<setfoundthing><title:>" + founding[i+1]
							+ "<type:>" + founding[i+2] + "<cardnumber:>"
							+ founding[i+3] + "<cardname:>" + founding[i+4]
							+ "<describption:>" + founding[i+5] + "<time:>"
							+ founding[i+6] + "<telephone:>" + founding[i+7]
							+ "<userid:>" + founding[i+8] + "<id:>" + founding[i];
					found = "[length=" + found.length() + "]" + found;
					String outStr = "<systemmassage><title:>有您的失物<massage:>"
							+ founding[i] + "<type:>0";
					outStr = "[length=" + outStr.length() + "]" + outStr;
					UDPServer udp = new UDPServer(found+outStr, address,
							Integer.parseInt(userIp[1]));
					udp.send();
				}
			}
		}
		return str;
	}

	/**
	 * TODO 获取命令列表
	 * 
	 * @return
	 */
	private String[] getCommand() {
		try {
			String[] command = get.getPublic();
			return command;
		} catch (ConfigurationException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * TODO 判断用户时候在线
	 * 
	 * @param userId
	 * @param read
	 * @return
	 */
	private boolean isStatus(String userId, DBRead read) {
		int i = read.getStatus(userId);
		if (i == 1) {
			return true;
		} else {
			return false;
		}
	}
}
