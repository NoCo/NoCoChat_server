package handle;

import java.util.ArrayList;
import java.util.List;

import dBase.DBRead;
import dBase.DBWrite;
import exception.ConfigurationException;
import file.Get;
/**列表操作类
 * 
 * @author jancojie
 *
 */
public class FriendsListHandle {
	Get get = null;
	DBWrite write = null;
	DBRead read = null;
	
	
	public FriendsListHandle(Get get, DBWrite write, DBRead read) {
		this.get = get;
		this.write = write;
		this.read = read;
	}
	
	/**接口方法
	 * 
	 * @param get
	 * @param write
	 * @param read
	 */
	public String getList(List<String> value, String command) throws Throwable {
		String[] str = getCommand();
		String valueString[] = value.toArray(new String[] {});
		if (command.equals(str[0]) || command == str[0]) {
			// 取得我的好友；
			String temp_1[] = getMyUserFriend(valueString);
			String temp = "";
			for (int i = 0; i < temp_1.length; i++) {
				temp += temp_1[i];
			}
			temp = "<getyourfriend>" + temp;
			temp = "[length=" + temp.length() + "]" + temp;
			return temp;

		} else if (command.equals(str[1]) || command == str[1]) {
			// 取得聊天室列表；
			String temp = getMyTRList(valueString);
			return temp;
		} else if (command.equals(str[2]) || command == str[2]) {
			// 取得聊天室成员；
			String temp = getMyTRFriend(valueString);
			return temp;
		} else if (command.equals(str[2]) || command == str[2]) {
			// 发送好友分组列表方法；
			String temp = getfriendgrouplist(valueString);
			temp = "[length=" + temp.length() + "]" + temp;
			return temp;
		}
		return null;

	}

	/**发送好友分组列表
	 * 
	 * @param valueString
	 * @return
	 */
	private String getfriendgrouplist(String[] valueString) {
		String userId=valueString[1];
		String[] friendgrouplist=read.getFriendsNameList(userId);
		List<String> output = new ArrayList<String>();
		for (int i = 0; i < friendgrouplist.length; i++) {
			output.add("<sendfriendgrouplist><userid:>"+userId+"<sname:>"+friendgrouplist[i]);
		}
		String str[] = output.toArray(new String[] {});
		String temp="";
		for (int i = 0; i < str.length; i++) {
			temp+=str[i];
		}
		
		return temp;
	}

	/**发送聊天室成员
	 * 
	 * @param valueString
	 * @return
	 */
	private String getMyTRFriend(String[] valueString) {
		String userId = valueString[1];
		String gId = valueString[3];
		String[] friends = read.getusergroupFriend_userId(gId);
		String[] gname = read.getusergroup_findGid(gId);
		List<String> output = new ArrayList<String>();
		for (int i = 0; i < friends.length; i++) {
			if (friends[i].equals(userId) || friends[i] == userId) {
				continue;
			}
			String name = read.getNickname(friends[i]);
			if (read.getStatus(friends[i]) == 1) {
				String friendIpPort[] = read.getIPPort(friends[i]);
				output.add("<userId:>" + friends[i] + "<name:>" + name
						+ "<ip:>" + friendIpPort[0] + "<port:>"
						+ friendIpPort[1]+"<talkroomid:>" + gId + "<talkroomname:>"
						+ gname[1] );
			} else {
				output.add("<userId:>" + friends[i] + "<name:>" + name
						+ "<ip:>_empty_<port:>0<talkroomid:>" + gId + "<talkroomname:>"
						+ gname[1] );
			}
		}
		String str[] = output.toArray(new String[] {});
		String temp = "";
		for (int i = 0; i < str.length; i++) {
			temp += str[i];
		}
		temp = "<getyourtalkroomuser>"+ temp;
		temp = "[length=" + temp.length() + "]" + temp;
		return temp;
	}
	
	/**发送聊天室列表
	 * 
	 * @param valueString
	 * @return
	 */
	private String getMyTRList(String[] valueString) {
		String userId = valueString[1];
		String[] talkrooms = read.getMYUserGroupList(userId);
		List<String> output = new ArrayList<String>();
		for (int i = 0; i < talkrooms.length; i += 2) {
			output.add("<talkroomid:>" + talkrooms[i] + "<talkroomname:>"
					+ talkrooms[i + 1]);
		}
		if (output.isEmpty()) {
			return null;
		} else {
			String str[] = output.toArray(new String[] {});
			String temp = "";
			for (int i = 0; i < str.length; i++) {
				temp += str[i];
			}
			temp = "<getyourtalkroom>" + temp;
			temp = "[length=" + temp.length() + "]" + temp;
			return temp;
		}

	}
	
	/**发送好友
	 * 
	 * @param valueString
	 * @return
	 */
	private String[] getMyUserFriend(String[] valueString) {
		String userId = valueString[1];
		String[] friendInfo = read.getFriends(userId);
		List<String> output = new ArrayList<String>();
		for (int i = 0; i < friendInfo.length; i += 5) {
			String nickname = read.getNickname(friendInfo[i]);
			if (read.getStatus(friendInfo[i]) == 1) {
				output.add("<fuserId:>" + friendInfo[i] + "<fname:>" + nickname
						+ "<extraName:>" + friendInfo[i + 2] + "<sname:>"
						+ friendInfo[i + 3] + "<ip:>" + friendInfo[i + 4]
						+ "<port:>" + friendInfo[i + 1]);
			} else {
				output.add("<fuserId:>" + friendInfo[i] + "<fname:>" + nickname
						+ "<extraName:>" + friendInfo[i + 2] + "<sname:>"
						+ friendInfo[i + 3] + "<ip:>_empty_<port:>0");
			}
		}
		String str[] = output.toArray(new String[] {});
		return str;
	}
	
	/**取得执行命令
	 * 
	 * @return
	 */
	private String[] getCommand() {
		try {
			String[] command = get.getFriendsList();
			return command;
		} catch (ConfigurationException e) {
			e.printStackTrace();
		}
		return null;
	}

}
