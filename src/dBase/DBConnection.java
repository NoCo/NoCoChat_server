package dBase;

import exception.ConfigurationException;

public class DBConnection {
	private PooledConnection conn=null;
	DBManager dbManager=null;
	public DBConnection(){
		try {
			conn=DBManager.getConnection();
			dbManager=DBManager.getInst();
		} catch (ConfigurationException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
	}
	public PooledConnection connection() {
		return conn;
	}
	public void close() {
		dbManager.close();
		
	}
}
