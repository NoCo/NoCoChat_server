package dBase;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.mysql.jdbc.Statement;
/**
* <p>Title: 数据库处理类</p>
* @author jancojie
* @version 1.0
*/
public class PooledConnection {

		private Connection connection = null;// 数据库连接

		private boolean busy ; // 此连接是否正在使用的标志，默认没有正在使用

		// 构造函数，根据一个 Connection 构造一个 PooledConnection 对象

		PooledConnection(Connection connection) {

			this.connection = connection;

		}
		/**获取一个preparedstatement对象
		 * 
		 * @param sql
		 * @return
		 * @throws SQLException
		 */
		public PreparedStatement getPrepareStatement(String sql) throws SQLException {
			return connection.prepareStatement(sql,Statement.RETURN_GENERATED_KEYS);
		}
		/**查询数据库
		 * 
		 * @param sql
		 * @return
		 * @throws SQLException
		 */
		public ResultSet executeQuery(String sql) throws SQLException {
			return connection.createStatement().executeQuery(sql);
		}
		
		/**更新数据库
		 * 
		 * @param sql
		 * @return
		 * @throws SQLException
		 */
		public int executeUpdate(String sql) throws SQLException {
			return connection.createStatement().executeUpdate(sql);
		}

		// 返回此对象中的连接

		Connection getConnection() {

			return connection;

		}

		// 设置此对象的，连接

		void setConnection(Connection connection) {

			this.connection = connection;

		}

		// 获得对象连接是否忙

		boolean isBusy() {

			return busy;

		}

		// 设置对象的连接正在忙

		void setBusy(boolean busy) {
			// System.out.println("set to busy:"+busy);
			this.busy = busy;

		}

		public void close() {
			busy = false;
		}



	}
