package dBase;

import java.sql.SQLException;

import dBase.PooledConnection;
import exception.ConfigurationException;
import file.Get;
/**
* <p>Title: 数据库连接管理类</p>
* @author jancojie
* @version 1.0
*/
public class DBManager {
	private static PooledConnection conn;
	private static ConnectionPool connectionPool;
	private static DBManager inst;
	public DBManager() throws ConfigurationException {
		
			connectionPool =new ConnectionPool(new Get().getMysql());
		try {	
			connectionPool.createPool();
			inst = this;
		} catch (Throwable e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
	}
	public static DBManager getInst() {
		return inst;
	}
	public static PooledConnection getConnection() throws ConfigurationException {
		if (inst == null)
			new DBManager();

		try {
			
			conn = connectionPool.getConnection();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return conn;
	}
	public void close() {
		try {
			connectionPool.closeConnectionPool();
		} catch (SQLException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
		
	}
}
