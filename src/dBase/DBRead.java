package dBase;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**数据库读取操作类
 * 
 * @author jancojie
 *
 */
public class DBRead  {
	ResultSet rs=null;
	private PooledConnection conn=null;
	public DBRead(DBConnection dbConnection) {
		this.conn=dbConnection.connection(); 
	}
/**获取自己所有暗恋对象
 * 
 * @param fromuserId
 * @return userId
 */
public String[] getmyhiddenlove(String fromuserId) {
	String sql="SELECT userId FROM hiddenlove"
			+ " WHERE fromuserId='"+fromuserId+"'";
	List<String>output =new ArrayList<String>();
	rs=getDB(sql);
	try {
		while (rs.next()) {
			output.add(rs.getString("userId"));
		}
		rs.close();
	} catch (Exception e) {
	}
	String[] str=output.toArray(new String[] {});
	return str;
	
}
/**获取某人被暗恋数
 * 
 * @param userId
 * @return count
 */
public int getOtherHiddenLoveCount(String userId) {
	String sql="SELECT fromuserId FROM hiddenlove"
			+ " WHERE userId='"+userId+"'";
	rs=getDB(sql);
	int count=0;
	try {
		while (rs.next()) {
			count++;
		}
		rs.close();
	} catch (Exception e) {
		// TODO: handle exception
	}
	return count;
	
}
	/**获取系统信息
	 * 
	 * @param userId
	 * @return title massage,type->0失物匹配 1->其它
	 */
	public String[] getSystemMassage(String userId) {
		String sql="SELECT title,massage,type "
				+ "FROM systemmassage WHERE userId='"+userId+"'";
		List<String>output=new ArrayList<String>();
		rs=getDB(sql);
		try {
			while (rs.next()) {
				output.add(rs.getString("title"));
				output.add(rs.getString("massage"));
				output.add(rs.getString("type"));
			}
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		String[] str=output.toArray(new String[] {});
		return str;
	}
	/**取得树洞回复
	 * 
	 * @param mainid
	 * @return no，replyID,nametype,massage,date
	 */
	public String[] gettressreply(String mainid) {
		String sql="SELECT no , replyID , massage , date ,nametype "
				+ "FROM treesreply WHERE mainid="+mainid;
		List<String>output=new ArrayList<String>();
		rs=getDB(sql);
		int i=0;
		try {
			while(rs.next()){
					output.add(rs.getString("no"));
					output.add(rs.getString("replyID"));
					output.add(rs.getString("nametype"));
					output.add(rs.getString("massage"));
					output.add(rs.getString("date"));
					
					i++;
			}
			output.add(""+i);
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
			}
			String str[] = output.toArray(new String[] {});
			return str;
		}	
	/**取得树洞回复(自己的回复)
	 * 
	 * @param mainid
	 * @return no,mainid,massage,date
	 */
	public String[] gettressreply_per(String replyID) {
		String sql="SELECT no , mainid , masaage , date "
				+ "FROM treesreply WHERE replyID='"+replyID+"'";
		List<String>output=new ArrayList<String>();
		rs=getDB(sql);
		try {
			while(rs.next()){
					output.add(rs.getString("no"));
					output.add(rs.getString("mainid"));
					output.add(rs.getString("massage"));
					output.add(rs.getString("date"));
			}
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
			}
			String str[] = output.toArray(new String[] {});
			return str;
		}	
	/**取得树洞主贴
	 * 
	 * @param 
	 * @return no，massage,date,ower,type,nametype
	 */
	public String[] gettress() {
		String sql="SELECT * FROM trees";
		List<String>output=new ArrayList<String>();
		rs=getDB(sql);
		try {
			while(rs.next()){
					output.add(rs.getString("no"));
					output.add(rs.getString("massage"));
					output.add(rs.getString("date"));
					output.add(rs.getString("ower"));
					output.add(rs.getString("type"));
					output.add(rs.getString("nametype"));
			}
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
			}
			String str[] = output.toArray(new String[] {});
			return str;
		}
	/**取得主贴ower
	 * 
	 * @param no
	 * @return ower
	 */
	public String gettressower(String no) {
		String sql="SELECT ower FROM trees WHERE no="+no;
		rs=getDB(sql);
		String out="";
		try {
			if(rs.next()){
				out=rs.getString("ower");
			}
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
			}
			return out;
		}	
	/**查找主贴是否存在
	 * 
	 * @param no
	 * @return
	 */
	public boolean isTress(String no) {
		String sql="SELECT no FROM trees WHERE no="+no;
		rs=getDB(sql);
		try {
			if(rs.next()){
				rs.close();
				return true;
			}else {
				rs.close();
				return false;
			}
		} catch (SQLException e) {
			e.printStackTrace();
				return false;
			}
			
		}	
	/**取得所有拾获
	 * 
	 * @param 
	 * @return no,title,type,cardnumber,cardname,describption,
	 * time,telephone，userId
	 */
	public String[] getfoundthing() {
		String sql="SELECT * FROM foundthing";
		List<String>output=new ArrayList<String>();
		rs=getDB(sql);
		try {
			while(rs.next()){
				output.add(rs.getString("no"));
				output.add(rs.getString("title"));
				output.add(rs.getString("type"));
				output.add(rs.getString("cardnumber"));
				output.add(rs.getString("cardname"));
				output.add(rs.getString("describption"));
				output.add(rs.getString("time"));
				output.add(rs.getString("telephone"));
				output.add(rs.getString("userId"));
			}
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
			}
			String str[] = output.toArray(new String[] {});
			return str;
		}	
	/**取得指定拾获
	 * 
	 * @param no
	 * @return no,title,type,cardnumber,cardname,describption,
	 * time,telephone，userId
	 */
	public String[] getfoundthing_per(String no) {
		String sql="SELECT * FROM foundthing WHERE no="+no;
		List<String>output=new ArrayList<String>();
		rs=getDB(sql);
		try {
			while(rs.next()){
				output.add(rs.getString("no"));
				output.add(rs.getString("title"));
				output.add(rs.getString("type"));
				output.add(rs.getString("cardnumber"));
				output.add(rs.getString("cardname"));
				output.add(rs.getString("describption"));
				output.add(rs.getString("time"));
				output.add(rs.getString("telephone"));
				output.add(rs.getString("userId"));
			}
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
			}
			String str[] = output.toArray(new String[] {});
			return str;
		}	
	/**取得所有失物
	 * 
	 * @param 
	 * @return no,title,type,cardnumber,cardname,describption,
	 * time,realneme,department,telephone,userId
	 */
	public String[] getlostthing() {
		String sql="SELECT * FROM lostthing";
		List<String>output=new ArrayList<String>();
		rs=getDB(sql);
		try {
			while(rs.next()){
				output.add(rs.getString("no"));
				output.add(rs.getString("title"));
				output.add(rs.getString("type"));
				output.add(rs.getString("cardnumber"));
				output.add(rs.getString("cardname"));
				output.add(rs.getString("describption"));
				output.add(rs.getString("time"));
				output.add(rs.getString("realname"));
				output.add(rs.getString("department"));
				output.add(rs.getString("telephone"));
				output.add(rs.getString("userId"));
			}
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
			}
			String str[] = output.toArray(new String[] {});
			return str;
		}
	/**获取个人信息表
	 * 
	 * @param userId
	 * @return sign,nickname,lovestatus,sex,personal
	 */
	public String[] getUserlnfo(String userId) {
		List<String>output=new ArrayList<String>();
		String sql="SELECT sign, nickname, loveStatus, sex, personal "
				+ "FROM userinfo WHERE userId='"+userId+"'";
			rs=getDB(sql);
				try {
					while(rs.next()){
					output.add(rs.getString("sign"));
					output.add(rs.getString("nickname"));
					output.add(rs.getString("loveStatus"));
					output.add(rs.getString("sex"));
					output.add(rs.getString("personal"));
					} 
					rs.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			String str[] = output.toArray(new String[] {});
			return str;
			}
	/**获取聊天室ID
	 * 
	 * @param userId
	 * @return
	 */
	public String[] getusergroup_findId(String userId) {
		List<String>output=new ArrayList<String>();
		String sql="SELECT gId "
				+ "FROM usergroup WHERE userId='"+userId+"'";
			rs=getDB(sql);
				try {
					while(rs.next()){
					output.add(rs.getString("gId"));
					} 
					rs.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			String str[] = output.toArray(new String[] {});
			return str;
			}
	/**获取聊天室Name
	 * 
	 * @param gId
	 * @return gname
	 */
	public String getusergroup_findgname(String gId) {
		String sql="SELECT gname "
				+ "FROM usergroup WHERE gId='"+gId+"'";
		String str="";
			rs=getDB(sql);
				try {
					if(rs.next()){
						str=rs.getString("gname");
					} 
					rs.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			return str;
			}
	/**获取聊天室列表gId
	 * 
	 * @param gId
	 * @return gId gname
	 */
	public String[] getusergroup_findGid(String gId) {
		List<String>output=new ArrayList<String>();
		String sql="SELECT gId , gname "
				+ " FROM usergroup WHERE gId='"+gId+"'";
			rs=getDB(sql);
				try {
					while(rs.next()){
					output.add(rs.getString("gId"));
					output.add(rs.getString("gname"));
					} 
					rs.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			String str[] = output.toArray(new String[] {});
			return str;
			}
	/**获取聊天室创建者id
	 * 
	 * @param gId
	 * @return userId
	 */
	public String[] getusergroup_findUserId(String gId) {
		List<String>output=new ArrayList<String>();
		String sql="SELECT userId"
				+ " FROM usergroup WHERE gId='"+gId+"'";
			rs=getDB(sql);
				try {
					while(rs.next()){
						output.add(rs.getString("userId"));
					} 
					rs.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			String str[] = output.toArray(new String[] {});
			return str;
			}
	/**获取聊天室列表
	 * 
	 * @param gname
	 * @return
	 */
	public String[] getusergroup() {
		List<String>output=new ArrayList<String>();
		String sql="SELECT gId , gname "
				+ "FROM usergroup ";
			rs=getDB(sql);
				try {
					while(rs.next()){
					output.add(rs.getString("gId"));
					output.add(rs.getString("gname"));
					} 
					rs.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			String str[] = output.toArray(new String[] {});
			return str;
			}
	/**获取聊天室列表gname
	 * 
	 * @param gname
	 * @return
	 */
	public String[] getusergroup_findGname(String gname) {
		List<String>output=new ArrayList<String>();
		String sql="SELECT gId , gname "
				+ "FROM usergroup WHERE gname='"+gname+"'";
			rs=getDB(sql);
				try {
					while(rs.next()){
					output.add(rs.getString("gId"));
					output.add(rs.getString("gname"));
					} 
					rs.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			String str[] = output.toArray(new String[] {});
			return str;
			}
	/**获取聊天室列表 gname gid
	 * 
	 * @param gname gid
	 * @return gid gname
	 */
	public String[] getusergroup_findGidGname(String gname,String gId) {
		List<String>output=new ArrayList<String>();
		String sql="SELECT gId , gname "
				+ "FROM usergroup WHERE gname='"+gname+"' and gId='"+gId+"'";
			rs=getDB(sql);
				try {
					while(rs.next()){
					output.add(rs.getString("gId"));
					output.add(rs.getString("gname"));
					} 
					rs.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			String str[] = output.toArray(new String[] {});
			return str;
			}
	/**获取个人群列表
	 * 
	 * @param gId 
	 * @return userId,gname
	 */
	public String[] getMYUserGroupList(String userId) {
		List<String>output=new ArrayList<String>();
		String sql="SELECT gId ,gname"
				+ " FROM usergroupfriend WHERE userId='"+userId+"'";
			rs=getDB(sql);
				try {
					while(rs.next()){
					output.add(rs.getString("gId"));
					output.add(rs.getString("gname"));
					} 
					rs.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			String str[] = output.toArray(new String[] {});
			return str;
			}
	/**获取群内成员
	 * 
	 * @param gId
	 * @return userId,gname
	 */
	public String[] getusergroupFriend(String gId) {
		List<String>output=new ArrayList<String>();
		String sql="SELECT userId ,gname"
				+ "FROM usergroupfriend WHERE gId='"+gId+"'";
			rs=getDB(sql);
				try {
					while(rs.next()){
					output.add(rs.getString("userId"));
					output.add(rs.getString("gname"));
					} 
					rs.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			String str[] = output.toArray(new String[] {});
			return str;
			}
	/**查询群是否被创建(自动分群)
	 * 
	 * @param userId
	 * @return 存在返回false 不存在返回true
	 */
	public boolean isUserGrounpExist(String gname) {
		String sql="SELECT gname "
				+ "FROM usergroup WHERE gname='"+gname+"'";
			rs=getDB(sql);
				try {
					if(rs.next()){
						rs.close();
						return false;
					}else{
						rs.close();
						return true;
					}
					
				}catch(SQLException e) {
					e.printStackTrace();
				}
				return false;

			}
	/**查询成员是否已被添加
	 * 
	 * @param userId
	 * @return false or true
	 */
	public boolean isUserGrounpFriend(String userId ,String gId) {
		String sql="SELECT userId "
				+ "FROM usergroupfriend WHERE gId='"+gId+"'and userId='"+userId+"'";
			rs=getDB(sql);
				try {
					if(rs.next()){
						rs.close();
						return false;
					}else{
						rs.close();
						return true;
					}
					
				}catch(SQLException e) {
					e.printStackTrace();
				}
				return false;

			}
	/**获取群内成员
	 * 
	 * @param gId
	 * @return userId
	 */
	public String[] getusergroupFriend_userId(String gId) {
		List<String>output=new ArrayList<String>();
		String sql="SELECT userId "
				+ "FROM usergroupfriend WHERE gId='"+gId+"'";
			rs=getDB(sql);
				try {
					while(rs.next()){
					output.add(rs.getString("userId"));
					} 
					rs.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			String str[] = output.toArray(new String[] {});
			return str;
			}
	/**获取个人信息表
	 * 
	 * @param userId
	 * @return nickname,sex
	 */
	public String[] getUserlnfo_nick_sex(String userId) {
		List<String>output=new ArrayList<String>();
		String sql="SELECT nickname , sex "
				+ "FROM userinfo WHERE userId='"+userId+"'";
			rs=getDB(sql);
				try {
					while(rs.next()){
					output.add(rs.getString("nickname"));;
					output.add(rs.getString("sex"));
					}
					rs.close();
				}catch (SQLException e) {
					e.printStackTrace();
				}
			String str[] = output.toArray(new String[] {});
			return str;
			}
	/**获取mysise信息表
	 * 
	 * @param userId
	 * @return major,rname,mysiseId,class,department
	 */	
	public String[] getMysise(String userId){
		List<String>output=new ArrayList<String>();
		String sql="SELECT major, rname, mysiseId, class, "
				+ "department FROM mysise"
				+ " WHERE userId= '"+userId+"'";
			rs=getDB(sql);
			try {
				while(rs.next()){
					output.add(rs.getString("major"));
					output.add(rs.getString("rname"));
					output.add(rs.getString("mysiseId"));
					output.add(rs.getString("class"));
					output.add(rs.getString("department"));
				}
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		String str[] = output.toArray(new String[] {});
		return str;
	}
	/**获取mysise真实姓名
	 * 
	 * @param mysiseId
	 * @return rname userId
	 */
	public String[] getMysise_name(String mysiseId){
		List<String>output=new ArrayList<String>();
		String sql="SELECT userId , rname FROM "
				+ "mysise WHERE mysiseId= '"+mysiseId+"'";
			rs=getDB(sql);
			try {
				while(rs.next()){
					output.add(rs.getString("rname"));
					output.add(rs.getString("userId"));
				}
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		String str[] = output.toArray(new String[] {});
		return str;
	}
	/**获取mysise信息表
	 * 
	 * @param userId
	 * @return major,department
	 */	
	public String[] getMysise_major_department(String userId){
		List<String>output=new ArrayList<String>();
		String sql="SELECT major, "
				+ "department FROM mysise"
				+ " WHERE userId= '"+userId+"'";
			rs=getDB(sql);
			try {
				while(rs.next()){
					output.add(rs.getString("major"));
					output.add(rs.getString("department"));
				}
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		String str[] = output.toArray(new String[] {});
		return str;
	}
	/**获取邮箱资料
	 * 
	 * @param userId
	 * @return
	 */
	public String[] getEmail(String userId){
		List<String>output=new ArrayList<String>();
		String sql="SELECT email,epwd FROM email"
				+ " WHERE userId='"+userId+"'";
			rs=getDB(sql);
			try {
				while(rs.next()){
					output.add(rs.getString("email"));
					output.add(rs.getString("epwd"));
				}
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
				
			}
		
		String str[] = output.toArray(new String[] {});
		return str;
	}
	/**获取所有用户id
	 * 
	 * @return 
	 */
	public String[] getUserInfoId() {
		String sql="SELECT userId FROM userinfo";
		List<String>output=new ArrayList<String>();
		try {
			rs=getDB(sql);
			while (rs.next()) {
				output.add(rs.getString("userId"));
			}
			rs.close();
		} catch (Throwable e) {
			e.printStackTrace();
		}
		String str[] = output.toArray(new String[] {});
		return str;
	}
	/**获取用户密码
	 * 
	 * @param userId
	 * @return
	 */
	public String getPwd(String userId) {
		String sql="SELECT pwd FROM userinfo"
				+ " WHERE userid='"+userId+"'";
		rs=getDB(sql);
		String pwd="";
		try {
			while(rs.next()){
				pwd=rs.getString("pwd");
			}
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
			}
			return pwd;
		}
	/**获取用户是否在线
	 * 
	 * @param userId
	 * @return
	 */
	public int getStatus(String userId) {
		String sql="SELECT status FROM signin"
				+ " WHERE userid='"+userId+"'";
		rs=getDB(sql);
		int status=0;
		try {
			while(rs.next()){
				status=rs.getInt("status");
			}
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
			}
			return status;
		}	
	/**获取Nickname
	 * 
	 * @param userId
	 * @return
	 */
	public String getNickname(String userId) {
		String sql="SELECT nickname FROM userinfo"
				+ " WHERE userid='"+userId+"'";
		rs=getDB(sql);
		String status="";
		try {
			while(rs.next()){
				status=rs.getString("nickname");
			}
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
			}
			return status;
		}	
	/**获取用户是否公开信息
	 * 
	 * @param userId
	 * @return
	 */
	public int getPersonal(String userId) {
		String sql="SELECT personal FROM userinfo"
				+ " WHERE userid='"+userId+"'";
		rs=getDB(sql);
		int status=0;
		try {
			while(rs.next()){
				status=rs.getInt("Personal");
			}
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
			}
			return status;
		}
	/**获取用户ip
	 * 
	 * @param userId
	 * @return ip port
	 */
	public String[] getIPPort(String userId) {
		String sql="SELECT ip ,port FROM signin"
				+ " WHERE userid='"+userId+"'";
		List<String>output=new ArrayList<String>();
		rs=getDB(sql);
		try {
			boolean flag=true;
			while(flag){
				if(rs.next()){
				output.add(rs.getString("ip"));
				output.add(rs.getString("port"));
				}
				else{
					flag=false;
				}
			}
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
			}
			String str[] = output.toArray(new String[] {});
			return str;
		}
	/**获取在线用户
	 * 
	 * @return userId，ip，port
	 */
	public String[] getstatusIpPort() {
		String sql="SELECT userId,ip ,port FROM signin"
				+ " WHERE status= 1";
		List<String>output=new ArrayList<String>();
		rs=getDB(sql);
		try {
			boolean flag=true;
			while(flag){
				if(rs.next()){
					output.add(rs.getString("userId"));
					output.add(rs.getString("ip"));
					output.add(rs.getString("port"));
				}
				else{
					flag=false;
				}
			}
			String str[] = output.toArray(new String[] {});
			rs.close();
			return str;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
			}
			
			
		}
	/**取得好友信息
	 * 
	 * @param userId
	 * @return fId,fport,extraName,sname,fIp
	 */
	public String[] getFriends(String userId) {
		String sql="SELECT fId , fport , extraName , sname ,fIp FROM friends "
				+ " WHERE userid='"+userId+"'";
		List<String>output=new ArrayList<String>();
		rs=getDB(sql);
		try {
			while(rs.next()){
				output.add(rs.getString("fId"));
				output.add(rs.getString("fport"));
				output.add(rs.getString("extraName"));
				output.add(rs.getString("sname"));
				output.add(rs.getString("fIp"));
			}
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
			}
			String str[] = output.toArray(new String[] {});
			return str;
		}
	/**判断好友是否存在
	 * 
	 * @param fuserId
	 * @param userId
	 * @return
	 */
	public boolean isFriends(String fuserId,String userId) {
		String sql="SELECT fId FROM friends "
				+ " WHERE userid='"+userId+"'and fId='"+fuserId+"'";
		rs=getDB(sql);
		try {
			if(rs.next()){
				rs.close();
				return false;
			}else {
				rs.close();
				return true;
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
			}
		return false;
		}
	/**获取好友分组列表
	 * 
	 * @param userId
	 * @return
	 */
	public String[] getFriendsNameList(String userId) {
		List<String>output=new ArrayList<String>();
		String sql="SELECT sname FROM friendgroup WHERE userId='"+userId+"'";
			rs=getDB(sql);
				try {
					while(rs.next()){
					output.add(rs.getString("sname"));
					} 
					rs.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			String str[] = output.toArray(new String[] {});
			return str;
			}
	/**获取缓存的好友
	 * 
	 * @param fId
	 * @return
	 */
	public String[] getGetFriend(String fId) {
		String sql="SELECT userId, name FROM getfriend "
				+ " WHERE fid='"+fId+"'";
		List<String>output=new ArrayList<String>();
		rs=getDB(sql);
		try {
			boolean flge=true;
			while(flge){
				if(rs.next()){
				output.add(rs.getString("userId"));
				output.add(rs.getString("name"));
				}
				else {
					flge=false;
				}
			}
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
			}
			String str[] = output.toArray(new String[] {});
			return str;
		}
	/**判断信息是否已经缓存
	 * 
	 * @param fId
	 * @return
	 */
	public boolean isgetGetFriend(String fId,String userid) {
		String sql="SELECT * FROM getfriend "
				+ " WHERE fid='"+fId+"'and userId='"+userid+"'";
		rs=getDB(sql);
		try {
			
				if(rs.next()){
					rs.close();
				 return false;
			
				}else {
					rs.close();
					return true;
				}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
			}
			return false;
		}
	/**判断暗恋时候匹配成功
	 * 
	 * @param fId
	 * @return
	 */
	public boolean isHiddenOk(String userId,String fromuserId) {
		String sql="SELECT * FROM hiddenlove "
				+ " WHERE fromuserId='"+fromuserId+"' and userId='"+userId+"'";
		rs=getDB(sql);
		try {
				if(rs.next()){
						rs.close();
						return true;
		
				}else {
					rs.close();
					return false;
				}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
			}
			return false;
		}
	/**获取缓存的聊天室好友
  * 
  * @param fId
  * @return userId(对方),fId(自己),gid,gname,fenlei
  */
	public String[] getGetgFriend(String fId) {
		String sql="SELECT userId , fId , gId , gname ,fenlei FROM getgfriendcache "
				+ " WHERE fId='"+fId+"'";
		List<String>output=new ArrayList<String>();
		rs=getDB(sql);
		try {
			boolean flge=true;
			while(flge){
				if(rs.next()){
				output.add(rs.getString("userId"));
				output.add(rs.getString("fId"));
				output.add(rs.getString("gId"));
				output.add(rs.getString("gname"));
				output.add(rs.getString("fenlei"));
				}
				else {
					flge=false;
				}
			}
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
			}
			String str[] = output.toArray(new String[] {});
			return str;
		}
	/**获取添加者好友缓存
	 * 
	 * @param userId
	 * @return
	 */
	public String[] getBackFriendCache(String userId) {
		String sql="SELECT fId, type FROM backfriendcache "
				+ " WHERE userId='"+userId+"'";
		List<String>output=new ArrayList<String>();
		rs=getDB(sql);
		try {
			boolean flge=true;
			while(flge){
				if(rs.next()){
				output.add(rs.getString("fId"));
				output.add(rs.getString("type"));
				}
				else {
					flge=false;
				}
			}
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
			}
			String str[] = output.toArray(new String[] {});
			return str;
		}
	/**获取添加者聊天室缓存
	 * 
	 * @param userId
	 * @return userId(自己),fId(对方),type,gid,gname,fenlei
	 */
	public String[] getBackgFriendCache(String userId) {
		String sql="SELECT userId, fId, type ,gId ,gname,fenlei FROM backgfriendcache "
				+ " WHERE userId='"+userId+"'";
		List<String>output=new ArrayList<String>();
		rs=getDB(sql);
		try {
			boolean flge=true;
			while(flge){
				if(rs.next()){
				output.add(rs.getString("userId"));	
				output.add(rs.getString("fId"));
				output.add(rs.getString("type"));
				output.add(rs.getString("gId"));
				output.add(rs.getString("gname"));
				output.add(rs.getString("fenlei"));
				}
				else {
					flge=false;
				}
			}
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
			}
			String str[] = output.toArray(new String[] {});
			return str;
		}
	/**获取个人信息表
	 * 
	 * @param userId
	 * @return info,date,name,userId,fname
	 */
	public String[] getInfoCache(String fId) {
		List<String>output=new ArrayList<String>();
		String sql="SELECT info , date , name , userId, fname "
				+ "FROM chatinfo WHERE fId='"+fId+"'";
			rs=getDB(sql);
				try {
					while(rs.next()){
					output.add(rs.getString("info"));
					output.add(rs.getString("date"));
					output.add(rs.getString("name"));
					output.add(rs.getString("userId"));
					output.add(rs.getString("fname"));
					} 
				}catch (SQLException e) {
					e.printStackTrace();
				}
			String str[] = output.toArray(new String[] {});
			return str;
			}
	/**数据库读取具体操作方法
	 * 
	 * @param sql
	 * @return
	 */
	public ResultSet getDB(String sql) {
		try {
			rs=conn.executeQuery(sql);
			conn.close();
		} catch (Throwable e) {
			e.printStackTrace();
		}
		return rs;
	}

}
