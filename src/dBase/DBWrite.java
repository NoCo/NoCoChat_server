package dBase;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.ibm.icu.text.SimpleDateFormat;
/**数据库写入操作类
 * 
 * @author ASIMO
 *
 */
public class DBWrite {
	Date now = new Date();
	private PooledConnection conn = null;
	public DBWrite(DBConnection dbConnection) {
		this.conn = dbConnection.connection();
	}
/**添加暗恋者
 * 
 * @param userId 对方id
 * @param fromuserId 自己id
 */
public void setHiddenLove(String userId,String fromuserId) {
	String sql="INSERT INTO hiddenlove (userId,fromuserId)"
			+ "value('"+userId+"','"+fromuserId+"')";
	try {
		conn.executeUpdate(sql);
	} catch (SQLException e) {
		// TODO 自动生成的 catch 块
		e.printStackTrace();
	}finally{
		conn.close();
	}	
}
/**取消暗恋者
 * 
 * @param userId
 * @param fromuserId
 */
public void deleteHiddenlove(String userId,String fromuserId){
	String sql="DELETE FROM hiddenlove WHERE userId='"+userId+
			"' and fromuserId='"+fromuserId+"'";
	try {
		conn.executeUpdate(sql);
	} catch (SQLException e) {
		// TODO 自动生成的 catch 块
		e.printStackTrace();
	}finally{
		conn.close();
	}	
	
	
}
/**添加好友缓存
 * 
 * @param userId
 * @param name
 * @param fId
 * @param fname
 */
	public void setGetFriends(String userId, String name, String fId,
			String fname) {
		String sql = "INSERT INTO getfriend(userId, name,"
				+ " fId, fname) VALUES ('" + userId + "','" + name + "'," + "'"
				+ fId + "','" + fname + "')";
		try {
			conn.executeUpdate(sql);
		} catch (Throwable e) {
			e.printStackTrace();
		}finally{
			conn.close();
		}

	}
	/**系统信息写入
	 * @param userId
	 * @param title
	 * @param massage
	 * @param type 0->失物匹配 1->
	 */
	public void setSystemMassage(String userId,String title,String massage,String type) {
		String sql="INSERT INTO systemmassage (userId , title , massage , type) VALUES (?,?,?,?)";
		try {
			PreparedStatement pstm=conn.getPrepareStatement(sql);
			pstm.setString(1, userId);
			pstm.setString(2, title);
			pstm.setString(3, massage);
			pstm.setString(4, type);
			pstm.executeUpdate();
		} catch (Exception e) {
			
		}finally{
			conn.close();
		}
		
	}
	/**系统信息缓存删除
	 * 
	 * @param userId
	 */
	public void deleteSystemMassage(String userId) {
		String sql = "DELETE FROM systemmassage WHERE userId='" + userId+"'";
		try {
			conn.executeUpdate(sql);
		} catch (Exception e) {
			
		}finally{
			conn.close();
		}
		
	}
	/**失物信息写入数据库
 * 
 * @param title
 * @param type
 * @param cardnumber
 * @param cardname
 * @param describption
 * @param time
 * @param realname
 * @param department
 * @param telephone
 * @param userId
 * @return key
 */
	public String setlostthing(String title, String type, String cardnumber,
			String cardname, String describption, String time, String realname,
			String department, String telephone, String userId) {
		String sql = "INSERT INTO `lostthing`(`title`, `type`, `cardnumber`, "
				+ "`cardname`, `describption`, `time`, `realname`, `department`, `telephone`,"
				+ " `userId`) VALUES (?,?,?,?,?,?,?,?,?,?)";
		String key=""; 
		try {
			PreparedStatement pstmt=conn.getPrepareStatement(sql);
			 pstmt.setString(1, title);
			 pstmt.setString(2, type);
			 pstmt.setString(3, cardnumber);
			 pstmt.setString(4, cardname);
			 pstmt.setString(5, describption);
			 pstmt.setString(6, time);
			 pstmt.setString(7, realname);
			 pstmt.setString(8, department);
			 pstmt.setString(9, telephone);
			 pstmt.setString(10, userId);
			 int row =pstmt.executeUpdate();
			 ResultSet rs=pstmt.getGeneratedKeys();
			 while (rs.next()) {
				  key= rs.getString(row);
			 }
			 conn.close();
			 return key;
			
		} catch (Throwable e) {
			e.printStackTrace();
			return key;
		}finally{
			conn.close();
		}
		
	}
	/**拾物信息写入数据库
* 
* @param title
* @param type
* @param cardnumber
* @param cardname
* @param describption
* @param time
* @param realname
* @param userId
* @return key
*/
	public String setfoundthing(String title, String type, String cardnumber,
		String cardname, String describption, 
		String time, String telephone, String userId) {
		String sql = "INSERT INTO `foundthing`(`title`, `type`, `cardnumber`, "
				+ "`cardname`, `describption`, `time`, `telephone`,"
				+ " `userId`) VALUES (?,?,?,?,?,?,?,?)";
		String key=""; 
		try {
			PreparedStatement pstmt=conn.getPrepareStatement(sql);
			 pstmt.setString(1, title);
			 pstmt.setString(2, type);
			 pstmt.setString(3, cardnumber);
			 pstmt.setString(4, cardname);
			 pstmt.setString(5, describption);
			 pstmt.setString(6, time);
			 pstmt.setString(7, telephone);
			 pstmt.setString(8, userId);
			 int row =pstmt.executeUpdate();
			 ResultSet rs=pstmt.getGeneratedKeys();
			 while (rs.next()) {
				  key= rs.getString(row);
			 }
			 return key;
				
		} catch (Throwable e) {
			e.printStackTrace();
			return key;
		}finally{
			conn.close();
		}
			
}
	/**树洞写入数据库
 * 
 * @param massage
 * @param date
 * @param ower
 * @return key
 */
	public String settrees(String massage,String date,String ower,String nametype) {
		String sql = "INSERT INTO `trees`(`massage`, `date`, `ower`,nametype)"
				+ " VALUES (?,?,?,?)";
		String key=""; 
		try {
			 PreparedStatement pstmt=conn.getPrepareStatement(sql);
			 pstmt.setString(1, massage);
			 pstmt.setString(2, date);
			 pstmt.setString(3, ower);
			 pstmt.setString(4, nametype);
			 pstmt.executeUpdate();
			 ResultSet rs=pstmt.getGeneratedKeys();
			if(rs.next()) {
				  key= rs.getString(1);
			 }
				rs.close();
			 return key;
				
		} catch (Throwable e) {
			e.printStackTrace();
			return key;
		}finally{
			conn.close();
		}
			
}
	/**树洞回复写入数据库
 * 
 * @param replyID
 * @param nametype
 * @param massage
 * @param date
 * @param mainid
 * @return
 */
	public String settreesreply(String replyID,String nametype,
		String massage,String date,String mainid) {
		String sql = "INSERT INTO `treesreply`(`replyID`, `massage`, `date`,`mainid`,nametype)"
				+ " VALUES (?,?,?,?,?)";
		String sql_1="UPDATE `trees` SET `type`='1' WHERE no="+mainid;
		String key=""; 
		try {
			PreparedStatement pstmt=conn.getPrepareStatement(sql);
			 pstmt.setString(1, replyID);
			 pstmt.setString(2, massage);
			 pstmt.setString(3,date );
			 pstmt.setString(4, mainid);
			 pstmt.setString(5, nametype);
			 int row =pstmt.executeUpdate();
			 ResultSet rs=pstmt.getGeneratedKeys();
			 while (rs.next()) {
				  key= rs.getString(row);
			 }
			 conn.executeUpdate(sql_1);
			 conn.close();
			 return key;
			
		} catch (Throwable e) {
			e.printStackTrace();
			return key;
		}
			
}
	/**添加聊天室好友缓存(邀请)
	 * 
	 * @param userId
	 * @param fId
	 * @param gId
	 * @param gname
	 */
	public void setGetgFriends(String userId, String fId, String gId,
			String gname) {
		String sql = "INSERT INTO getgfriendcache ( userId, fId,"
				+ " gId, gname) VALUES ('" + userId + "','" + fId + "'," + "'"
				+ gId + "','" + gname + "')";
		try {
			conn.executeUpdate(sql);
			conn.close();
		} catch (Throwable e) {
			e.printStackTrace();
		}

	}
	/**添加聊天室好友缓存(添加)
	 * 
	 * @param userId
	 * @param fId
	 * @param gId
	 * @param gname
	 * @param fenlei
	 */
	public void setGetgFriends_add(String userId, String fId, String gId,
			String gname,String fenlei) {
		String sql = "INSERT INTO getgfriendcache ( userId, fId,"
				+ " gId, gname, fenlei) VALUES ('" + userId + "','" + fId + "'," + "'"
				+ gId + "','" + gname + "','"+fenlei+"')";
		try {
			conn.executeUpdate(sql);
			conn.close();
		} catch (Throwable e) {
			e.printStackTrace();
		}

	}
	/**删除添加好友缓存
 * 
 * @param fId
 */
	public void deleteGetFriends(String fId) {
		String sql = "DELETE FROM getfriend WHERE fId='" + fId + "'";
		try {
			conn.executeUpdate(sql);
			conn.close();
		} catch (Throwable e) {
			e.printStackTrace();
		}

	}
	/**删除添加好友返回缓存
 * 
 * @param fId
 */
	public void deleteGetgFriends(String fId) {
		String sql = "DELETE FROM getgfriendcache WHERE fId='" + fId + "'";
		try {
			conn.executeUpdate(sql);
			conn.close();
		} catch (Throwable e) {
			e.printStackTrace();
		}

	}
	/**删除信息缓存
 * 
 * @param fId
 */
	public void deleteChatInfo(String fId) {
		String sql = "DELETE FROM chatinfo WHERE fId='" + fId + "'";
		try {
			conn.executeUpdate(sql);
			conn.close();
		} catch (Throwable e) {
			e.printStackTrace();
		}

	}
	/**添加好友返回缓存
 * 
 * @param userId
 * @param fId
 * @param type
 */
	public void setBackFriendCache(String userId, String fId, String type) {
		String sql = "INSERT INTO backfriendcache(userId, fId,"
				+ "type) VALUES ('" + userId + "','" + fId + "','" + type
				+ "')";
		try {
			conn.executeUpdate(sql);
			conn.close();
		} catch (Throwable e) {
			e.printStackTrace();
		}

	}
	/**添加聊天室好友返回缓存(添加)
	 * 
	 * @param userId
	 * @param fId
	 * @param type
	 * @param gId
	 * @param gname
	 * @param fenlei
	 */
	public void setBackgFriendCache_add(String userId, String fId, String type,
			String gId, String gname ,String fenlei) {
		String sql = "INSERT INTO backgfriendcache(userId, fId,"
				+ "type, gId , gname ,fenlei) VALUES ('" + userId + "'," + "'" + fId
				+ "','" + type + "','" + gId + "','" + gname + "','" + fenlei+ "')";
		try {
			conn.executeUpdate(sql);
			conn.close();
		} catch (Throwable e) {
			e.printStackTrace();
		}

	}
	/**添加聊天室好友返回缓存(邀请)
 * 
 * @param userId
 * @param fId
 * @param type
 * @param gId
 * @param gname
 */
	public void setBackgFriendCache(String userId, String fId, String type,
			String gId, String gname) {
		String sql = "INSERT INTO backgfriendcache(userId, fId,"
				+ "type, gId , gname) VALUES ('" + userId + "'," + "'" + fId
				+ "','" + type + "','" + gId + "','" + gname + "')";
		try {
			conn.executeUpdate(sql);
			conn.close();
		} catch (Throwable e) {
			e.printStackTrace();
		}

	}
	/**删除添加好友返回缓存
 * 
 * @param userId
 */
	public void deleteBackFriendCache(String userId) {
		String sql = "DELETE FROM backfriendcache WHERE userId='" + userId
				+ "'";
		try {
			conn.executeUpdate(sql);
			conn.close();
		} catch (Throwable e) {
			e.printStackTrace();
		}

	}
	/**删除失物帖子
	 * 
	 * @param no
	 * @return 
	 */
	public boolean deletelostthing(String no) {
		String sql = "DELETE FROM lostthing WHERE no='" + no
				+ "'";
		try {
			conn.executeUpdate(sql);
			conn.close();
			return true;
		} catch (Throwable e) {
			e.printStackTrace();
			conn.close();
			return false;
		}
	}
	/**删除失物帖子
	 * 
	 * @param no
	 * @return 
	 */
	public boolean deletefoundthing(String no) {
		String sql = "DELETE FROM foundthing WHERE no='" + no
				+ "'";
		try {
			conn.executeUpdate(sql);
			conn.close();
			return true;
		} catch (Throwable e) {
			e.printStackTrace();
			conn.close();
			return false;
		}
}
	/**删除树洞主贴
 * @param no
 * @return 
 */
	public boolean deletetrees(String no) {
	String sql = "DELETE FROM trees WHERE no='" + no
			+ "'";
	try {
		conn.executeUpdate(sql);
		conn.close();
		return true;
	} catch (Throwable e) {
		conn.close();
		e.printStackTrace();
		return false;
		
	}
}
	/**删除树洞回复帖子、
 * 
 * @param no
 * @return 
 */
	public boolean deletetreesreply(String no) {
	String sql = "DELETE FROM treesreply WHERE no=" + no;
	try {
		conn.executeUpdate(sql);
		conn.close();
		return true;
	} catch (Throwable e) {
		e.printStackTrace();
		conn.close();
		return false;
	}
}
	/**删除添加聊天室好友返回缓存
	 * 
	 * @param userId
	 */
	public void deleteBackgFriendCache(String userId) {
		String sql = "DELETE FROM backgfriendcache WHERE userId='" + userId
				+ "'";
		try {
			conn.executeUpdate(sql);
			conn.close();
		} catch (Throwable e) {
			e.printStackTrace();
		}

	}
	/**创建分组
	 * @param userId
	 * @param sname
	 */
	public void insertFriendGroup(String userId, String sname) {
		String sql = "INSERT INTO friendgroup(userId ,sname) " + "VALUES ('"
				+ userId + "','" + sname + "')";
		try {
			conn.executeUpdate(sql);
			conn.close();
		} catch (Throwable e) {
			e.printStackTrace();
		}

	}
	/**修改分组名 
	 * @param userId
	 * @param sname
	 */
	public void updateFriendGroup(String userId, String sname, String snamenew) {
		String sql = "UPDATE friendgroup SET sname='" + snamenew
				+ "' WHERE sname='" + sname + "' and userId='" + userId + "'";
		String sql_1 = "UPDATE friends SET sname='" + snamenew
				+ "' WHERE userId='" + userId + "' and sname='" + sname + "'";
		try {
			conn.executeUpdate(sql);
			conn.executeUpdate(sql_1);
			conn.close();
		} catch (Throwable e) {
			e.printStackTrace();
		}

	}
	/**修改分组名(好友)
	 * @param userId
	 * @param sname
	 */
	public void updateFriendsname(String userId, String fId, String sname,
			String snamenew) {
		String sql = "UPDATE friends SET sname='" + snamenew
				+ "' WHERE userId='" + userId + "' and sname='" + sname
				+ "' and fId='" + fId + "'";
		try {
			conn.executeUpdate(sql);
			conn.close();
		} catch (Throwable e) {
			e.printStackTrace();
		}

	}
	/**修改好友备注名
	 * @param userId
	 * @param fId
	 * @param extraname
	 */
	public void updateFriendname(String userId, String fId, String extraname) {
		String sql = "UPDATE friends SET extraname='" + extraname
				+ "' WHERE userId='" + userId + "' and fId='" + fId + "'";
		try {
			conn.executeUpdate(sql);
			conn.close();
		} catch (Throwable e) {
			e.printStackTrace();
		}

	}
	/**添加mysise信息
 * 
 * @param userId
 * @param major
 * @param rname
 * @param mysiseId
 * @param mclass
 * @param department
 * @return
 */
	public boolean setmysise(String userId, String major, String rname,
			String mysiseId, String mclass, String department) {
		String sql = "UPDATE mysise SET major='" + major + "' , rname='"
				+ rname + "', " + "mysiseId='" + mysiseId + "', class='" + mclass
				+ "', " + "department='" + department + "'WHERE userId='"
				+ userId + "'";
		try {
			conn.executeUpdate(sql);
			conn.close();
			return true;
		} catch (Throwable e) {
			e.printStackTrace();
			return false;
		}

	}
	/** email信息登记(初始登记)
	 * @param userId
	 * @param email
	 * @return
	 */
	public boolean setEmail_update(String userId, String email) {
		String sql = "UPDATE email SET email='" + email + "' WHERE userId='"
				+ userId + "'";

		try {
			conn.executeUpdate(sql);
			conn.close();
			return true;
		} catch (Throwable e) {
			e.printStackTrace();
			return false;
		}

	}
	/**email信息登记(绑定)
	 * @param userId
	 * @param email
	 * @param pwd
	 * @return
	 */
	public boolean setBandEmail_update(String userId, String email, String pwd) {
		String sql = "UPDATE email SET email='" + email + "', epwd='" + pwd
				+ "' WHERE userId='" + userId + "'";

		try {
			conn.executeUpdate(sql);
			conn.close();
			return true;
		} catch (Throwable e) {
			e.printStackTrace();
			return false;
		}

	}
	/**修改邮箱信息
 * 
 * @param userId
 * @param email
 * @param epwd
 * @return
 */
	public boolean setEmail(String userId, String email, String epwd) {
		String sql = "UPDATE email SET email='" + email + "',epwd='" + epwd
				+ "' WHERE userId='" + userId + "'";

		try {
			conn.executeUpdate(sql);
			conn.close();
			return true;
		} catch (Throwable e) {
			e.printStackTrace();
			return false;
		}

	}
	/**修改用户信息
 * 
 * @param userId
 * @param sign
 * @param nickname
 * @param loveStatus
 * @param sex
 * @param personal
 * @return
 */
	public boolean setUserlnfo(String userId, String sign, String nickname,
			int loveStatus, int sex, int personal) {
		String sql = "UPDATE userinfo SET sign='" + sign + "' ," + "nickname='"
				+ nickname + "', loveStatus=" + loveStatus + ", " + "sex="
				+ sex + ", personal=" + personal + " WHERE userId='" + userId
				+ "'";
		try {
			conn.executeUpdate(sql);
			conn.close();
			return true;
		} catch (Throwable e) {
			e.printStackTrace();
			return false;
		}

	}
	/**修改用户信息
 * 
 * @param userId
 * @param sign
 * @param nickname
 * @param loveStatus
 * @param sex
 * @param personal
 * @return
 */
	public boolean setUserlnfo_control(String userId, String pwd, String nickname) {
		String sql = "UPDATE userinfo SET nickname='"
				+ nickname +  "',pwd='"+pwd+"' WHERE userId='" + userId+ "'";
		try {
			conn.executeUpdate(sql);
			conn.close();
			return true;
		} catch (Throwable e) {
			e.printStackTrace();
			return false;
		}

	}
	/**用户登录
 * 
 * @param userId
 * @param ip
 * @param port
 */
	public void setSignIn(String userId, String ip, int port) {
		int status = 1;
		String date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(now);
		String sql = "UPDATE signin SET date='" + date + "',ip='" + ip
				+ "',port=" + port + ",status=" + status + " WHERE userId='"
				+ userId + "'";
		String sql_1 = "UPDATE friends SET fip = '" + ip + "' , " + "fport ='"
				+ (Integer.toString(port)) + "' WHERE fId='" + userId + "'";
		try {
			conn.executeUpdate(sql);
			conn.executeUpdate(sql_1);
			conn.close();
		} catch (Throwable e) {
			e.printStackTrace();
		}

	}
	/**修改用户在线状态
 * 
 * @param userId
 * @param status
 */
	public void setStatus(String userId,int status) {
		String sql = "UPDATE signin SET status=" + status + " WHERE userId='"
				+ userId + "'";
		try {
			conn.executeUpdate(sql);
			conn.close();
		} catch (Throwable e) {
			e.printStackTrace();
		}

	}
	/**用户下线
 * 
 * @param userId
 * @return
 */
	public boolean setDownLine(String userId) {
		int status = 0;
		String sql = "UPDATE signin SET status=" + status + " WHERE userId='"
				+ userId + "'";
		try {
			conn.executeUpdate(sql);
			conn.close();
			return true;
		} catch (Throwable e) {
			e.printStackTrace();
			return false;
		}

	}
	/**添加信息缓存
 * 
 * @param info
 * @param date
 * @param userId
 * @param name
 * @param fId
 * @param fname
 * @return
 */
	public boolean setChatInfo(String info, String date, String userId,
			String name, String fId, String fname) {
		String sql = "INSERT INTO chatinfo(info, date,userId, "
				+ "name, fId, fname) VALUES ('" + info + "','" + date + "'"
				+ ",'" + userId + "','" + name + "','" + fId + "','" + fname
				+ "')";
		try {
			conn.executeUpdate(sql);
			conn.close();
			return true;
		} catch (Throwable e) {
			e.printStackTrace();
			return false;
		}

	}
	/**添加聊天室
	 * @param userId
	 * @param gId
	 * @param gname
	 * @return
	 */
	public boolean setUserGroup(String userId, String gname, String gId) {
		String date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(now);
		String sql = "INSERT INTO usergroup(userId, date,gname, "
				+ "gId) VALUES ('" + userId + "','" + date + "'" + ",'" + gname
				+ "','" + gId + "')";
		String sql_1 = "INSERT INTO usergroupfriend(userId , gId, "
				+ "gname) VALUES ('" + userId + "','" + gId + "','" + gname
				+ "')";
		try {
			conn.executeUpdate(sql);
			conn.executeUpdate(sql_1);
			conn.close();
			return true;
		} catch (Throwable e) {
			e.printStackTrace();
			return false;
		}

	}
	/**添加聊天室成员
	 * 
	 * @param userId
	 * @param gId
	 * @param gname
	 * @return
	 */
	public boolean setUserGroupFriend(String userId, String gId, String gname) {
		String sql = "INSERT INTO usergroupfriend(userId , gId, "
				+ "gname) VALUES ('" + userId + "','" + gId + "','" + gname
				+ "')";
		try {
			conn.executeUpdate(sql);
			conn.close();
			return true;
		} catch (Throwable e) {
			e.printStackTrace();
			return false;
		}

	}
	/**添加好友
 * 
 * @param userId
 * @param fId
 * @param nickName
 * @param ip
 * @param port
 * @param ip_2
 * @param port_2
 * @param name
 * @return
 */
	public boolean setFriends(String userId, String fId, String nickName,
			String ip, String port, String ip_2, String port_2, String name) {
		String date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(now);
		String sql = "INSERT INTO friends(fId, extraName,sdate, "
				+ "userId, fip, fport) VALUES ('" + fId + "','" + nickName
				+ "'" + ",'" + date + "','" + userId + "','" + ip + "','"
				+ port + "')";
		String sql_1 = "INSERT INTO friends(fId, extraName,sdate, "
				+ "userId, fip, fport) VALUES ('" + userId + "','" + name + "'"
				+ ",'" + date + "','" + fId + "','" + ip_2 + "','" + port_2
				+ "')";
		try {
			conn.executeUpdate(sql);
			conn.executeUpdate(sql_1);
			conn.close();
			return true;
		} catch (Throwable e) {
			e.printStackTrace();
			return false;
		}

	}
	/**添加用户
 * 
 * @param userId
 * @param pwd
 * @param nickname
 */
	public void setLogin(String userId, String pwd, String nickname) {
		String sql = "INSERT INTO userinfo(userId, pwd, nickname) "
				+ "VALUES ('" + userId + "','" + pwd + "','" + nickname + "')";
		String sql_1 = "INSERT INTO signin(userId) VALUES('" + userId + "')";
		String sql_2 = "INSERT INTO email(userId) VALUES('" + userId + "')";
		String sql_3 = "INSERT INTO mysise(userId) VALUES('" + userId + "')";
		String sql_4 = "INSERT INTO file(userId) VALUES('" + userId + "')";
		String sql_5 = "INSERT INTO friendgroup(userId ,sname) " + "VALUES ('"
				+ userId + "','我的好友')";
		String sql_6 = "INSERT INTO friendgroup(userId ,sname) " + "VALUES ('"
				+ userId + "','陌生人')";
		String sql_7 = "INSERT INTO friendgroup(userId ,sname) " + "VALUES ('"
				+ userId + "','黑名单')";
		try {
			conn.executeUpdate(sql);
			conn.executeUpdate(sql_1);
			conn.executeUpdate(sql_2);
			conn.executeUpdate(sql_3);
			conn.executeUpdate(sql_4);
			conn.executeUpdate(sql_5);
			conn.executeUpdate(sql_6);
			conn.executeUpdate(sql_7);
			conn.close();
		} catch (Throwable e) {
			e.printStackTrace();
		}

	}
}
