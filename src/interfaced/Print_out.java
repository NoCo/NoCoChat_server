package interfaced;

import java.awt.Point;
import java.io.PrintStream;
import java.io.OutputStream;

import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import org.eclipse.swt.widgets.Display;

public class Print_out extends PrintStream {
	    private JTextArea textArea;
	    private StringBuffer sb = new StringBuffer();
	    private JScrollPane jScrollPane;
	    public Print_out(OutputStream out, JTextArea textArea,JScrollPane jScrollPane){
	        super(out);
	        this.textArea = textArea;
	        this.jScrollPane=jScrollPane;
	    }	    
	    public void write(byte[] buf, int off, int len) {
	        final String message = new String(buf, off, len);
	        Display.getDefault().asyncExec(new Runnable(){
	            @Override
	            public void run() {
	            	sb.append(message);
	            	textArea.setText(sb.toString());
	            	 int height=20;
	            	    Point p = new Point();
	            	    p.setLocation(0,textArea.getLineCount()*height);
	            	    jScrollPane.getViewport().setViewPosition(p);
	            }
	        });
	    }
	}

