package interfaced;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.layout.FillLayout;

import handle.Control;

import java.awt.Frame;

import org.eclipse.swt.awt.SWT_AWT;

import dBase.DBShwo;

import java.awt.BorderLayout;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.JTabbedPane;


import javax.swing.JLabel;
import javax.swing.SwingConstants;

import java.awt.Font;

import javax.swing.JTextField;

import org.eclipse.wb.swing.FocusTraversalOnArray;

import java.awt.Component;

import javax.swing.JButton;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class UserInfoManager extends Shell {
	private JTable table;
	private JTextField userId;
	private JTextField pwd;
	private JTextField nickname;
	private JTextField status;
	/**
	 * Create the shell.
	 * @param display
	 */
	public UserInfoManager(Display display) {
		super(display, SWT.CLOSE | SWT.MIN);
		setSize(610, 506);
		setLayout(new FillLayout(SWT.HORIZONTAL));
		
		Composite composite = new Composite(this, SWT.EMBEDDED);
		
		Frame frame = SWT_AWT.new_Frame(composite);
		
		String[] head=new String[] {
				"userId", "pwd", "nickname", "status"
			};
		String sql="SELECT `userinfo`.userid,`pwd`,`nickname`"
				+ ",`status` FROM `userinfo`,`signin` "
				+ "WHERE `userinfo`.userid=`signin`.userid";
		DBShwo show=new DBShwo(head.length);
		Object[][] data=show.getUserInfo(sql);
		
		JTabbedPane tp = new JTabbedPane(JTabbedPane.TOP);
		frame.add(tp, BorderLayout.NORTH);
		table = new JTable();
		table.setFillsViewportHeight(true);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.setModel(new DefaultTableModel(data,head));
		JScrollPane scrollPane = new JScrollPane(table);
		tp.addTab("信息查看",null,scrollPane,null);
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		JPanel jPanel=new JPanel();
		tp.addTab("信息修改",null,jPanel,null);
		tp.setEnabledAt(1, true);
		jPanel.setLayout(null);
		
		JLabel lblUserid = new JLabel("userId:");
		lblUserid.setFont(new Font("Dialog", Font.BOLD, 16));
		lblUserid.setHorizontalAlignment(SwingConstants.CENTER);
		lblUserid.setBounds(12, 39, 75, 30);
		jPanel.add(lblUserid);
		
		userId = new JTextField();
		userId.setBounds(97, 45, 129, 21);
		jPanel.add(userId);
		userId.setColumns(10);
		
		JLabel lblPwd = new JLabel("PWD\uFF1A");
		lblPwd.setHorizontalAlignment(SwingConstants.CENTER);
		lblPwd.setFont(new Font("Dialog", Font.BOLD, 16));
		lblPwd.setBounds(286, 39, 75, 30);
		jPanel.add(lblPwd);
		
		pwd = new JTextField();
		pwd.setColumns(10);
		pwd.setBounds(371, 45, 129, 21);
		jPanel.add(pwd);
		
		JLabel lblNickname = new JLabel("NickName:\r\n");
		lblNickname.setHorizontalAlignment(SwingConstants.CENTER);
		lblNickname.setFont(new Font("Dialog", Font.BOLD, 16));
		lblNickname.setBounds(35, 125, 93, 30);
		jPanel.add(lblNickname);
		
		nickname = new JTextField();
		nickname.setBounds(185, 130, 223, 24);
		jPanel.add(nickname);
		nickname.setColumns(10);
		
		JLabel lblStusas = new JLabel("Status:\r\n");
		lblStusas.setHorizontalAlignment(SwingConstants.CENTER);
		lblStusas.setFont(new Font("Dialog", Font.BOLD, 16));
		lblStusas.setBounds(35, 223, 93, 30);
		jPanel.add(lblStusas);
		
		status = new JTextField();
		status.setBounds(185, 229, 223, 21);
		jPanel.add(status);
		status.setColumns(10);
		
		JLabel label = new JLabel("1\u4E3A\u5728\u7EBF\uFF0C0\u4E3A\u79BB\u7EBF");
		label.setBounds(430, 232, 107, 15);
		jPanel.add(label);
		
		JButton button = new JButton("\u4FEE\u6539");
		button.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if(status.getText().equals("0")||status.getText().equals("1")){
					String []userInfo={userId.getText(),pwd.getText(),nickname.getText(),status.getText()};
					Control control=new Control();
					control.updateUserType(userInfo);
				}else {
					JOptionPane.showMessageDialog(null, "status只能输入1/0", "输入错误", JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		button.setBounds(250, 326, 93, 23);
		jPanel.add(button);
		jPanel.setFocusTraversalPolicy(new FocusTraversalOnArray(new Component[]{lblUserid, userId, lblPwd, pwd, lblNickname, nickname, lblStusas, status, label}));
	}

	/**
	 * Create contents of the shell.
	 */
	protected void createContents() {
		setText("userInfoManager");
		setSize(600, 450);

	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}
}
