package interfaced;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;

import server.Server;

import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.widgets.Composite;

import handle.Control;

import java.awt.Frame;
import java.io.FileOutputStream;
import java.io.PrintStream;

import org.eclipse.swt.awt.SWT_AWT;

import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.wb.swt.SWTResourceManager;
import org.eclipse.swt.widgets.Text;



public class MainIter {
	private static final FormToolkit formToolkit = new FormToolkit(Display.getDefault());
	private static Text sysMassage_title;
	private static Text sysMassage_massage;

	/**
	 * Launch the application.
	 * @param args
	 * @throws Throwable 
	 */
	public static void main(String[] args) throws Throwable {
		final Display display = Display.getDefault();
		Shell shlSisechatManagerSystem = new Shell(display,SWT.MIN);
		shlSisechatManagerSystem.setMinimumSize(new Point(800, 640));
		shlSisechatManagerSystem.setSize(450, 300);
		shlSisechatManagerSystem.setText("siseChat manager system");
		final Server server=new Server();
		shlSisechatManagerSystem.setLayout(new FormLayout());
		
		Group group_1 = new Group(shlSisechatManagerSystem, SWT.NONE);
		group_1.setLayout(new FillLayout(SWT.HORIZONTAL));
		FormData fd_group_1 = new FormData();
		fd_group_1.bottom = new FormAttachment(100, -27);
		fd_group_1.right = new FormAttachment(100, -10);
		group_1.setLayoutData(fd_group_1);
		Button btnNewButton = new Button(group_1, SWT.NONE);
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
				 server.start();
			}
		});
		btnNewButton.setText("\u542F\u52A8\u670D\u52A1\u5668");
		
		Group group_2 = new Group(shlSisechatManagerSystem, SWT.NONE);
		fd_group_1.left = new FormAttachment(group_2, 6);
		FormData fd_group_2 = new FormData();
		fd_group_2.left = new FormAttachment(0, 10);
		fd_group_2.right = new FormAttachment(100, -296);
		fd_group_2.top = new FormAttachment(0, 347);
		fd_group_2.bottom = new FormAttachment(100, -27);
		group_2.setLayoutData(fd_group_2);
		
		Group group = new Group(shlSisechatManagerSystem, SWT.NONE);
		fd_group_1.top = new FormAttachment(group, 6);
		FormData fd_group = new FormData();
		fd_group.top = new FormAttachment(100, -602);
		fd_group.bottom = new FormAttachment(100, -261);
		fd_group.left = new FormAttachment(0, 10);
		fd_group.right = new FormAttachment(100, -10);
		
		Button button_1 = new Button(group_1, SWT.NONE);
		button_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
				System.exit(0);
			}
		});
		button_1.setText("\u5173\u95ED\u670D\u52A1\u5668");
		group.setLayout(new FillLayout(SWT.HORIZONTAL));
		group.setLayoutData(fd_group);
		
		Composite composite = new Composite(group, SWT.EMBEDDED);
		composite.setLayout(new FillLayout(SWT.HORIZONTAL));
		
		Frame frame = SWT_AWT.new_Frame(composite);
		final JTextArea textArea = new JTextArea();
		textArea.setLineWrap(true);
		textArea.setEditable(false);
		JScrollPane scrollPane = new JScrollPane(textArea);
		scrollPane.setBounds(0, 305, 748, -303);
		frame.add(scrollPane);
		System.setOut(new Print_out(System.out,textArea,scrollPane));
		
		System.setErr(new PrintStream(new FileOutputStream("errlog.log")));
		
		Button btnNewButton_2 = new Button(group_2, SWT.NONE);
		btnNewButton_2.addMouseListener(new MouseAdapter() {
			
			public void mouseDown(MouseEvent e) {
				textArea.setText("");
			}
		});
		btnNewButton_2.setBounds(10, 57, 98, 27);
		btnNewButton_2.setText("\u6E05\u5C4F");
		
		Button button = formToolkit.createButton(group_2, "\u76F8\u5173\u8BBE\u7F6E", SWT.NONE);
		button.setBounds(134, 57, 98, 27);
		
		Label label = new Label(group_2, SWT.NONE);
		label.setFont(SWTResourceManager.getFont("黑体", 24, SWT.NORMAL));
		label.setAlignment(SWT.CENTER);
		label.setBounds(75, 10, 345, 33);
		formToolkit.adapt(label, true, true);
		label.setText("\u529F\u80FD\u83DC\u5355");
		
		Button button_2 = new Button(group_2, SWT.NONE);
		button_2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
				UserInfoManager user=new UserInfoManager(display);
				user.open();
			}
		});
		button_2.setBounds(258, 57, 90, 27);
		formToolkit.adapt(button_2, true, true);
		button_2.setText("\u7528\u6237\u7BA1\u7406");
		
		Button button_3 = new Button(group_2, SWT.NONE);
		button_3.setBounds(367, 57, 90, 27);
		formToolkit.adapt(button_3, true, true);
		button_3.setText("\u4FE1\u606F\u7BA1\u7406");
		
		Button btnNewButton_1 = new Button(group_2, SWT.NONE);
		btnNewButton_1.setBounds(10, 100, 98, 27);
		formToolkit.adapt(btnNewButton_1, true, true);
		btnNewButton_1.setText("\u804A\u5929\u5BA4\u7BA1\u7406");
		
		Button btnNewButton_3 = new Button(group_2, SWT.NONE);
		btnNewButton_3.setBounds(134, 100, 98, 27);
		formToolkit.adapt(btnNewButton_3, true, true);
		btnNewButton_3.setText("\u6811\u6D1E\u7BA1\u7406");
		
		Button btnNewButton_4 = new Button(group_2, SWT.NONE);
		btnNewButton_4.setBounds(258, 100, 90, 27);
		formToolkit.adapt(btnNewButton_4, true, true);
		btnNewButton_4.setText("\u5931\u7269\u7BA1\u7406");
		
		Button button_4 = new Button(group_2, SWT.NONE);
		button_4.setBounds(367, 100, 90, 27);
		formToolkit.adapt(button_4, true, true);
		button_4.setText("\u62FE\u7269\u7BA1\u7406");
		
		sysMassage_title = new Text(group_2, SWT.BORDER);
		sysMassage_title.setBounds(88, 141, 240, 30);
		formToolkit.adapt(sysMassage_title, true, true);
		
		Button send = new Button(group_2, SWT.NONE);
		send.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
				String title=sysMassage_title.getText();
				String massage=sysMassage_title.getText();
				Control control=new Control();
				try {
					control.sendSysMassage(title, massage);
				} catch (Throwable e1) {
					// TODO 自动生成的 catch 块
					e1.printStackTrace();
				}
				
			}
		});
		send.setBounds(367, 174, 80, 27);
		formToolkit.adapt(send, true, true);
		send.setText("\u53D1\u9001");
		
		Label label_1 = new Label(group_2, SWT.NONE);
		label_1.setBounds(10, 144, 61, 17);
		formToolkit.adapt(label_1, true, true);
		label_1.setText("\u9898\u76EE\uFF1A");
		
		Label label_2 = new Label(group_2, SWT.NONE);
		label_2.setBounds(10, 206, 61, 17);
		formToolkit.adapt(label_2, true, true);
		label_2.setText("\u5185\u5BB9\uFF1A");
		
		sysMassage_massage = new Text(group_2, SWT.BORDER);
		sysMassage_massage.setBounds(88, 203, 240, 30);
		formToolkit.adapt(sysMassage_massage, true, true);
		shlSisechatManagerSystem.open();
		shlSisechatManagerSystem.layout();
		while (!shlSisechatManagerSystem.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}
}
