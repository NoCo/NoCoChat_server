package exception;
/**
	* <p>Title: 设置文件存取异常类</p>
	* @author jancojie
	* @version 1.0
	*/
public class ConfigurationException extends Exception{
	
	private static final long serialVersionUID = -7766876821449351359L;
	public ConfigurationException(){} 
	public ConfigurationException(String msg){ 
	super(msg); 
	} 
}
