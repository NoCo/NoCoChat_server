package link;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UDPServer {
	public static final int MAX_LEN = 1024;
	private String outStr = null;
	private DatagramSocket client = null;
	private int port;
	private InetAddress ip;

	public UDPServer() {
		
	}
	public UDPServer(String outStr,InetAddress ip,int port) {
		this.outStr = outStr;
		this.ip = ip;
	    this.port=port;

	}

	public String getOutStr() {
		return outStr;
	}

	public void setOutStr(String outStr) {
		this.outStr = outStr;
	}
/**UDP协议主动连接
 * @return
 * @throws Throwable 
 * @throws Exception
 */
	public String[] server() throws Throwable  {
		client=new DatagramSocket(9800);
		DatagramPacket out =null; 
		DatagramPacket in = null;
		byte inByte[] = new byte[MAX_LEN];
		byte outByte[] = new byte[MAX_LEN];
		outByte = outStr.getBytes("UTF-8");
		out=new DatagramPacket(outByte, outByte.length,ip,port);
		client.send(out);
		System.out.println("UDP发送完成...");
		String instr="";
		List<String> input = new ArrayList<String>();
		boolean flag = true;
		in=new DatagramPacket(inByte, inByte.length);
		client.receive(in);
		while (flag) {
				instr = new String(in.getData(), 0, in.getLength(), "UTF-8");
			if (!(instr.isEmpty())) {
				System.out.println("UDP接收完成，正在处理...");
				flag = false;
			}
			
		}
		client.close();
		List<String> outpList = new ArrayList<String>();
		outpList = changeString(instr,null);
		String srt[] = outpList.toArray(new String[] {});
		for (int i = 0; i < srt.length; i++) {
			String temp = getCommande(srt[i]);
			input=getValue(temp);
		}
		String output[] = input.toArray(new String[] {});
		return output;
	}
	
	/**UDP协议主动发送
	 * @throws Throwable 
	 * 
	 * @throws Throwable
	 */
	public void send() throws Throwable {
		client=new DatagramSocket();
		DatagramPacket out =null; 
		byte outByte[] = new byte[MAX_LEN];
		outByte = outStr.getBytes("UTF-8");
		out=new DatagramPacket(outByte, outByte.length,ip,port);
		client.send(out);
		System.out.println("UDP发送完成...");
		client.close();
	}
/**	
 * <p>
 * 读取请求的长度，进行自动分包（私有）
 * <p>
 * 
 * @param dataInput
 * @return str
 */
private List<String> changeString(String input, List<String> outputlist) {
	String temp = "";
	if (outputlist == null) {
		outputlist = new ArrayList<String>();
	}
	if (!(temp.isEmpty())) {
		input += temp;
	}
	String output = "";
	String pattern = "(?<=\\[length=)(\\d+)(?=\\])";
	int length;
	Pattern p = Pattern.compile(pattern);
	Matcher m = p.matcher(input);
	if (m.find()) {// 寻找匹配项
		// 获取消息字符串实际应有的长度
		length = Integer.parseInt(m.group(1));

		int startindex = input.indexOf("]") + 1;
		output = input.substring(startindex);

		if (output.length() == length) {
			outputlist.add(output);
			temp = "";
		} else if (output.length() < length) {
			temp = output;
		} else if (output.length() > length) {
			outputlist.add(output.substring(0, length));
			temp = "";
			input = input.substring(startindex + length);
			changeString(input, outputlist);
		}
	} else {
		temp = input;
	}

	return outputlist;
}
/**
 * 读取动作指令
 * 
 * @param str
 * @return strCommande
 */
private String getCommande(String str) {
	String strCommande = "";
	String pattern = "(?<=<)(\\w+)(?=>)";
	Pattern p = Pattern.compile(pattern);
	Matcher m = p.matcher(str);
	if (m.find()) {// 寻找匹配项
		strCommande = str.substring(m.end() + 1, str.length());
	}
	return strCommande;
}

/**
 * 读取具体属性
 * 
 * @param str
 * @return strValue
 */
private List<String> getValue(String str) {
	List<String> strValue = new ArrayList<String>();
	String pattern = "(?<=<)(\\w+:)(?=>)|(?<=>)([^><]+)";
	Pattern p = Pattern.compile(pattern);
	Matcher m = p.matcher(str);
	while (m.find()) {
		// 寻找匹配项
		strValue.add(m.group());
	}
	return strValue;
}
public int getPort() {
	return port;
}
public void setPort(int port) {
	this.port = port;
}
public InetAddress getIp() {
	return ip;
}
public void setIp(InetAddress ip) {
	this.ip = ip;
}

}
