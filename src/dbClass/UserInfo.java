/**
 * 
 */
package dbClass;

/**
 * 项目名称：HGCF_SiseChat_Server
 * 包的名称：dbClass
 * 类名称：UserInfo
 * 类描述：数据库表映射类
 * 作者：NoCo_关颖杰
 * 创建时间：2014年9月10日上午11:06:40
 * 版本：1.0
 */
public class UserInfo {
  private String userId;
  private String pwd;
  private String sign;
  private String photoID;
  private String nickname;
  private int loveStatud;
  private int sex;
  private int personal;
/**
 * @return userId
 */
public String getUserId() {
	return userId;
}
/**
 * @param userId 要设置的 userId
 */
public void setUserId(String userId) {
	this.userId = userId;
}
/**
 * @return pwd
 */
public String getPwd() {
	return pwd;
}
/**
 * @param pwd 要设置的 pwd
 */
public void setPwd(String pwd) {
	this.pwd = pwd;
}
/**
 * @return sign
 */
public String getSign() {
	return sign;
}
/**
 * @param sign 要设置的 sign
 */
public void setSign(String sign) {
	this.sign = sign;
}
/**
 * @return photoID
 */
public String getPhotoID() {
	return photoID;
}
/**
 * @param photoID 要设置的 photoID
 */
public void setPhotoID(String photoID) {
	this.photoID = photoID;
}
/**
 * @return nickname
 */
public String getNickname() {
	return nickname;
}
/**
 * @param nickname 要设置的 nickname
 */
public void setNickname(String nickname) {
	this.nickname = nickname;
}
/**
 * @return loveStatud
 */
public int getLoveStatud() {
	return loveStatud;
}
/**
 * @param loveStatud 要设置的 loveStatud
 */
public void setLoveStatud(int loveStatud) {
	this.loveStatud = loveStatud;
}
/**
 * @return sex
 */
public int getSex() {
	return sex;
}
/**
 * @param sex 要设置的 sex
 */
public void setSex(int sex) {
	this.sex = sex;
}
/**
 * @return personal
 */
public int getPersonal() {
	return personal;
}
/**
 * @param personal 要设置的 personal
 */
public void setPersonal(int personal) {
	this.personal = personal;
}
  
}
