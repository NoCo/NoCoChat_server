package dbClass;

public class Mysise {
	private String userId;
	private String major;
	private String rname;
	private String mysiseId;
	private String m_class;
	private String department;
	/**
	 * @return userId
	 */
	public String getUserId() {
		return userId;
	}
	/**
	 * @param userId 要设置的 userId
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}
	/**
	 * @return major
	 */
	public String getMajor() {
		return major;
	}
	/**
	 * @param major 要设置的 major
	 */
	public void setMajor(String major) {
		this.major = major;
	}
	/**
	 * @return rname
	 */
	public String getRname() {
		return rname;
	}
	/**
	 * @param rname 要设置的 rname
	 */
	public void setRname(String rname) {
		this.rname = rname;
	}
	/**
	 * @return mysiseId
	 */
	public String getMysiseId() {
		return mysiseId;
	}
	/**
	 * @param mysiseId 要设置的 mysiseId
	 */
	public void setMysiseId(String mysiseId) {
		this.mysiseId = mysiseId;
	}
	/**
	 * @return class
	 */
	public String getM_class() {
		return m_class;
	}
	/**
	 * @param class1 要设置的 class
	 */
	public void setClass(String class1) {
		m_class = class1;
	}
	/**
	 * @return department
	 */
	public String getDepartment() {
		return department;
	}
	/**
	 * @param department 要设置的 department
	 */
	public void setDepartment(String department) {
		this.department = department;
	}
}
