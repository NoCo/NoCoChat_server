package dbClass;

public class Friends {
	private int no;
	private String fId;
	private String extraName;
	private String sname;
	private String sdate;
	private String userId;
	private String fIp;
	private String fport;
	/**
	 * @return no
	 */
	public int getNo() {
		return no;
	}
	/**
	 * @param no 要设置的 no
	 */
	public void setNo(int no) {
		this.no = no;
	}
	/**
	 * @return fId
	 */
	public String getfId() {
		return fId;
	}
	/**
	 * @param fId 要设置的 fId
	 */
	public void setfId(String fId) {
		this.fId = fId;
	}
	/**
	 * @return extraName
	 */
	public String getExtraName() {
		return extraName;
	}
	/**
	 * @param extraName 要设置的 extraName
	 */
	public void setExtraName(String extraName) {
		this.extraName = extraName;
	}
	/**
	 * @return sname
	 */
	public String getSname() {
		return sname;
	}
	/**
	 * @param sname 要设置的 sname
	 */
	public void setSname(String sname) {
		this.sname = sname;
	}
	/**
	 * @return sdate
	 */
	public String getSdate() {
		return sdate;
	}
	/**
	 * @param sdate 要设置的 sdate
	 */
	public void setSdate(String sdate) {
		this.sdate = sdate;
	}
	/**
	 * @return userId
	 */
	public String getUserId() {
		return userId;
	}
	/**
	 * @param userId 要设置的 userId
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}
	/**
	 * @return fIp
	 */
	public String getfIp() {
		return fIp;
	}
	/**
	 * @param fIp 要设置的 fIp
	 */
	public void setfIp(String fIp) {
		this.fIp = fIp;
	}
	/**
	 * @return fport
	 */
	public String getFport() {
		return fport;
	}
	/**
	 * @param fport 要设置的 fport
	 */
	public void setFport(String fport) {
		this.fport = fport;
	}
}
