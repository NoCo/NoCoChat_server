package dbClass;

public class FriendGroup {
	private int no;
	private String userIdString;
	private String sname;
	/**
	 * @return no
	 */
	public int getNo() {
		return no;
	}
	/**
	 * @param no 要设置的 no
	 */
	public void setNo(int no) {
		this.no = no;
	}
	/**
	 * @return userIdString
	 */
	public String getUserIdString() {
		return userIdString;
	}
	/**
	 * @param userIdString 要设置的 userIdString
	 */
	public void setUserIdString(String userIdString) {
		this.userIdString = userIdString;
	}
	/**
	 * @return sname
	 */
	public String getSname() {
		return sname;
	}
	/**
	 * @param sname 要设置的 sname
	 */
	public void setSname(String sname) {
		this.sname = sname;
	}
	
	
}
