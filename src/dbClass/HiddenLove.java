package dbClass;

public class HiddenLove {
	private int no;
	private String userId;
	private String fromuserId;
	/**
	 * @return no
	 */
	public int getNo() {
		return no;
	}
	/**
	 * @param no 要设置的 no
	 */
	public void setNo(int no) {
		this.no = no;
	}
	/**
	 * @return userId
	 */
	public String getUserId() {
		return userId;
	}
	/**
	 * @param userId 要设置的 userId
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}
	/**
	 * @return fromuserId
	 */
	public String getFromuserId() {
		return fromuserId;
	}
	/**
	 * @param fromuserId 要设置的 fromuserId
	 */
	public void setFromuserId(String fromuserId) {
		this.fromuserId = fromuserId;
	}
}
