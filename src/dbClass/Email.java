/**
 * 
 */
package dbClass;

/**
 * @author Jancojie
 *
 */
public class Email {
	private String userId;
	private String email;
	private String epwd;
	/**
	 * @return userId
	 */
	public String getUserId() {
		return userId;
	}
	/**
	 * @param userId 要设置的 userId
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}
	/**
	 * @return email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @param email 要设置的 email
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @return epwd
	 */
	public String getEpwd() {
		return epwd;
	}
	/**
	 * @param epwd 要设置的 epwd
	 */
	public void setEpwd(String epwd) {
		this.epwd = epwd;
	}
}
