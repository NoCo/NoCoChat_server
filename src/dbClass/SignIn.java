package dbClass;

import java.util.Date;
/**登录信息保存类
 * 
 * @author jancojie
 *
 */
public class SignIn {
	private int no;
	private String userId;
	private String ip;
	private int port;
	private Date date;
	private int status;
	/**
	 * @return no
	 */
	public int getNo() {
		return no;
	}
	/**
	 * @param no 要设置的 no
	 */
	public void setNo(int no) {
		this.no = no;
	}
	/**
	 * @return userId
	 */
	public String getUserId() {
		return userId;
	}
	/**
	 * @param userId 要设置的 userId
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}
	/**
	 * @return ip
	 */
	public String getIp() {
		return ip;
	}
	/**
	 * @param ip 要设置的 ip
	 */
	public void setIp(String ip) {
		this.ip = ip;
	}
	/**
	 * @return port
	 */
	public int getPort() {
		return port;
	}
	/**
	 * @param port 要设置的 port
	 */
	public void setPort(int port) {
		this.port = port;
	}
	/**
	 * @return date
	 */
	public Date getDate() {
		return date;
	}
	/**
	 * @param date 要设置的 date
	 */
	public void setDate(Date date) {
		this.date = date;
	}
	/**
	 * @return status
	 */
	public int getStatus() {
		return status;
	}
	/**
	 * @param status 要设置的 status
	 */
	public void setStatus(int status) {
		this.status = status;
	}
	
	
}