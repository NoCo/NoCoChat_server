package dbClass;

public class Lyric {
	private int id;
	private String src;
	private String userId;
	private int musicid;
	/**
	 * @return id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id 要设置的 id
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return src
	 */
	public String getSrc() {
		return src;
	}
	/**
	 * @param src 要设置的 src
	 */
	public void setSrc(String src) {
		this.src = src;
	}
	/**
	 * @return userId
	 */
	public String getUserId() {
		return userId;
	}
	/**
	 * @param userId 要设置的 userId
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}
	/**
	 * @return musicid
	 */
	public int getMusicid() {
		return musicid;
	}
	/**
	 * @param musicid 要设置的 musicid
	 */
	public void setMusicid(int musicid) {
		this.musicid = musicid;
	}
}
