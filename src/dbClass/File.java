package dbClass;

public class File {
	private int no;
	private String filename;
	private int size ;
	private String userId;
	/**
	 * @return no
	 */
	public int getNo() {
		return no;
	}
	/**
	 * @param no 要设置的 no
	 */
	public void setNo(int no) {
		this.no = no;
	}
	/**
	 * @return filename
	 */
	public String getFilename() {
		return filename;
	}
	/**
	 * @param filename 要设置的 filename
	 */
	public void setFilename(String filename) {
		this.filename = filename;
	}
	/**
	 * @return size
	 */
	public int getSize() {
		return size;
	}
	/**
	 * @param size 要设置的 size
	 */
	public void setSize(int size) {
		this.size = size;
	}
	/**
	 * @return userId
	 */
	public String getUserId() {
		return userId;
	}
	/**
	 * @param userId 要设置的 userId
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}
}
