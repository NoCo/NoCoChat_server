package dbClass;

public class Game {
	private int id;
	private String userid;
	private String mark;
	/**
	 * @return id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id 要设置的 id
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return userid
	 */
	public String getUserid() {
		return userid;
	}
	/**
	 * @param userid 要设置的 userid
	 */
	public void setUserid(String userid) {
		this.userid = userid;
	}
	/**
	 * @return mark
	 */
	public String getMark() {
		return mark;
	}
	/**
	 * @param mark 要设置的 mark
	 */
	public void setMark(String mark) {
		this.mark = mark;
	}
}
