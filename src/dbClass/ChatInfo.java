/**
 * 
 */
package dbClass;

/**
 * @author Jancojie
 *
 */
public class ChatInfo {
	private int no;
	private String info;
	private String date;
	private String userId;
	private String name;
	private String fId;
	private String fname;
	/**
	 * @return no
	 */
	public int getNo() {
		return no;
	}
	/**
	 * @param no 要设置的 no
	 */
	public void setNo(int no) {
		this.no = no;
	}
	/**
	 * @return info
	 */
	public String getInfo() {
		return info;
	}
	/**
	 * @param info 要设置的 info
	 */
	public void setInfo(String info) {
		this.info = info;
	}
	/**
	 * @return date
	 */
	public String getDate() {
		return date;
	}
	/**
	 * @param date 要设置的 date
	 */
	public void setDate(String date) {
		this.date = date;
	}
	/**
	 * @return userId
	 */
	public String getUserId() {
		return userId;
	}
	/**
	 * @param userId 要设置的 userId
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}
	/**
	 * @return name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name 要设置的 name
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return fId
	 */
	public String getfId() {
		return fId;
	}
	/**
	 * @param fId 要设置的 fId
	 */
	public void setfId(String fId) {
		this.fId = fId;
	}
	/**
	 * @return fname
	 */
	public String getFname() {
		return fname;
	}
	/**
	 * @param fname 要设置的 fname
	 */
	public void setFname(String fname) {
		this.fname = fname;
	}
}
