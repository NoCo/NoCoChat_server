package dbClass;

public class Music {
	private int id;
	private String name;
	private String src;
	private String img;
	private String userid;
	private String repository;
	/**
	 * @return id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id 要设置的 id
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name 要设置的 name
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return src
	 */
	public String getSrc() {
		return src;
	}
	/**
	 * @param src 要设置的 src
	 */
	public void setSrc(String src) {
		this.src = src;
	}
	/**
	 * @return img
	 */
	public String getImg() {
		return img;
	}
	/**
	 * @param img 要设置的 img
	 */
	public void setImg(String img) {
		this.img = img;
	}
	/**
	 * @return userid
	 */
	public String getUserid() {
		return userid;
	}
	/**
	 * @param userid 要设置的 userid
	 */
	public void setUserid(String userid) {
		this.userid = userid;
	}
	/**
	 * @return repository
	 */
	public String getRepository() {
		return repository;
	}
	/**
	 * @param repository 要设置的 repository
	 */
	public void setRepository(String repository) {
		this.repository = repository;
	}
}
