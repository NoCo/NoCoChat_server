package dbClass;

public class LostThing {
	private int no;
	private String title;
	private char type;
	private String cardnumber;
	private String cardname;
	private String describption;
	private String time;
	private String realname;
	private String department;
	private String telephone;
	private String userId;
	/**
	 * @return no
	 */
	public int getNo() {
		return no;
	}
	/**
	 * @param no 要设置的 no
	 */
	public void setNo(int no) {
		this.no = no;
	}
	/**
	 * @return title
	 */
	public String getTitle() {
		return title;
	}
	/**
	 * @param title 要设置的 title
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	/**
	 * @return type
	 */
	public char getType() {
		return type;
	}
	/**
	 * @param type 要设置的 type
	 */
	public void setType(char type) {
		this.type = type;
	}
	/**
	 * @return cardnumber
	 */
	public String getCardnumber() {
		return cardnumber;
	}
	/**
	 * @param cardnumber 要设置的 cardnumber
	 */
	public void setCardnumber(String cardnumber) {
		this.cardnumber = cardnumber;
	}
	/**
	 * @return cardname
	 */
	public String getCardname() {
		return cardname;
	}
	/**
	 * @param cardname 要设置的 cardname
	 */
	public void setCardname(String cardname) {
		this.cardname = cardname;
	}
	/**
	 * @return describption
	 */
	public String getDescribption() {
		return describption;
	}
	/**
	 * @param describption 要设置的 describption
	 */
	public void setDescribption(String describption) {
		this.describption = describption;
	}
	/**
	 * @return time
	 */
	public String getTime() {
		return time;
	}
	/**
	 * @param time 要设置的 time
	 */
	public void setTime(String time) {
		this.time = time;
	}
	/**
	 * @return realname
	 */
	public String getRealname() {
		return realname;
	}
	/**
	 * @param realname 要设置的 realname
	 */
	public void setRealname(String realname) {
		this.realname = realname;
	}
	/**
	 * @return department
	 */
	public String getDepartment() {
		return department;
	}
	/**
	 * @param department 要设置的 department
	 */
	public void setDepartment(String department) {
		this.department = department;
	}
	/**
	 * @return telephone
	 */
	public String getTelephone() {
		return telephone;
	}
	/**
	 * @param telephone 要设置的 telephone
	 */
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	/**
	 * @return userId
	 */
	public String getUserId() {
		return userId;
	}
	/**
	 * @param userId 要设置的 userId
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}
}
