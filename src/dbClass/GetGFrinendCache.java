package dbClass;

public class GetGFrinendCache {
	private int no;
	private String userId;
	private String fId;
	private String gId;
	private String gname;
	private char fenlei;
	/**
	 * @return no
	 */
	public int getNo() {
		return no;
	}
	/**
	 * @param no 要设置的 no
	 */
	public void setNo(int no) {
		this.no = no;
	}
	/**
	 * @return userId
	 */
	public String getUserId() {
		return userId;
	}
	/**
	 * @param userId 要设置的 userId
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}
	/**
	 * @return fId
	 */
	public String getfId() {
		return fId;
	}
	/**
	 * @param fId 要设置的 fId
	 */
	public void setfId(String fId) {
		this.fId = fId;
	}
	/**
	 * @return gId
	 */
	public String getgId() {
		return gId;
	}
	/**
	 * @param gId 要设置的 gId
	 */
	public void setgId(String gId) {
		this.gId = gId;
	}
	/**
	 * @return gname
	 */
	public String getGname() {
		return gname;
	}
	/**
	 * @param gname 要设置的 gname
	 */
	public void setGname(String gname) {
		this.gname = gname;
	}
	/**
	 * @return fenlei
	 */
	public char getFenlei() {
		return fenlei;
	}
	/**
	 * @param fenlei 要设置的 fenlei
	 */
	public void setFenlei(char fenlei) {
		this.fenlei = fenlei;
	}
}
