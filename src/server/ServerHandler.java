package server;


import java.util.Date;

import com.ibm.icu.text.SimpleDateFormat;

import handle.Prehandle;
import event.EventAdapter;

/**
 * <p>Title: 服务端事件处理器</p>
 * <p>Description: 处理服务器端受理客户请示的各类事件, 在这里实现具体应用</p>
 * @author jancojie
 * @version 1.0
 */
public class ServerHandler extends EventAdapter {
		Prehandle preh = null;
		Date now = new Date();
    public ServerHandler() {
    }

    public void onAccept() throws Exception {
        System.out.print("接收到新的连接请求...\r\n");
    }

    public void onAccepted(Request request) throws Exception {
        System.out.print("请求已授予...\r\n");
    }

    public void onRead(Request request) throws Throwable {
        byte[] data = request.getDataInput();
        System.out.print("已接收到命令...\r\n");
        System.out.println(new String(data,"utf8"));
        preh=new Prehandle();
		request.attach(preh.assignHandle(data));
    }

    public void onWrite(Request request, Response response) throws Exception {
        System.out.print("命令处理完毕，返回中...\r\n");
        byte output[]=(byte[])request.attachment();
        System.out.println(new String(output,"utf-8"));
        if(output.length>0){  	
        	response.send(output);
        }
        
    }

    public void onClosed(Request request) throws Exception {
        System.out.print("连接终止...\r\n");
    }

    public void onError(String error) {
    	String date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(now);
        System.err.print("#onAError: " + error+" time:"+date+"\r\n");
    }
}
