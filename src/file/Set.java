package file;

import exception.ConfigurationException;
/**
* <p>Title: 创建各设置文件类</p>
* @author jancojie
* @version 1.0
*/

public class Set {
	Configuration config = null;
	public void setPorts(String channelPorts 
			,String TCPports,String UDPports )
					throws ConfigurationException {
		config=new Configuration();
		config.setValue("ChannelPorts", channelPorts);
		config.setValue("TCPports", TCPports);
		config.setValue("UDPports", UDPports);
		config.saveFile("ports.ini", "Monitor interface Settings");
	}

	public void setMysql(String jdbcDriver,String dburl 
			,String dbUsername ,String dbPassword ) 
					throws ConfigurationException {
		config=new Configuration();
		config.setValue("jdbcDriver", jdbcDriver);
		config.setValue("dburl", dburl);
		config.setValue("dbUsername", dbUsername);
		config.setValue("dbPassword", dbPassword);
		config.saveFile("mysql.ini", "Set the database related data");
	}
	public void setCommand(String logIn,String signIn,
			String add,String chatInfo,String downline,
			String mysise,String email,String information,
			String file) throws ConfigurationException {
		config=new Configuration();
		config.setValue("logIn", logIn);
		config.setValue("signIn", signIn);
		config.setValue("add", add);
		config.setValue("chatInfo", chatInfo);
		config.setValue("downline", downline);
		config.setValue("mysise", mysise);
		config.setValue("email", email);
		config.setValue("information", information);
		config.setValue("file", file);
		config.saveFile("getCommand.ini");
		
	}
}