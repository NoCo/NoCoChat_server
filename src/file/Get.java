package file;


import exception.ConfigurationException;
/**
* <p>Title: 获取设置文件的相关属性类</p>
* @author jancojie
* @version 1.0
*/
public class Get {
	 Configuration config=null;
	 public int[] getPorts()throws ConfigurationException {
		config=new Configuration("ports.ini");
		String temp=config.getValue("ChannelPorts");
		String ps[]=temp.split(",");
		int[] num=new int [ps.length];
		for(int i=0;i<ps.length;i++){
			num[i]=Integer.parseInt(ps[i]);
		}
		 return num;
	}
	public String[] getMysql() throws ConfigurationException {
		config=new Configuration("mysql.ini");
		String[] mysql={config.getValue("jdbcDriver"),config.getValue("dburl")
				,config.getValue("dbUsername"),config.getValue("dbPassword")};
		return mysql;
	}
	public String[] getLogin() throws ConfigurationException {
		config=new Configuration("getCommand.ini");
		String temp=config.getValue("logIn");
		String str[]= temp.split(",");
		return str;
	}
	public String[] getSignIn() throws ConfigurationException {
		config=new Configuration("getCommand.ini");
		String temp=config.getValue("signIn");
		String str[]= temp.split(",");
		return str;
	}
	public String[] getAdd() throws ConfigurationException {
		config=new Configuration("getCommand.ini");
		String temp=config.getValue("add");
		String str[]= temp.split(",");
		return str;
	}
	public String[] getFriendsList() throws ConfigurationException {
		config=new Configuration("getCommand.ini");
		String temp=config.getValue("friendsList");
		String str[]= temp.split(",");
		return str;
	}
	public String[] getDownLine() throws ConfigurationException {
		config=new Configuration("getCommand.ini");
		String temp=config.getValue("downline");
		String str[]= temp.split(",");
		return str;
	}
	public String[] getMysise() throws ConfigurationException {
		config=new Configuration("getCommand.ini");
		String temp=config.getValue("mysise");
		String str[]= temp.split(",");
		return str;
	}
	public String[] getInformation() throws ConfigurationException {
		config=new Configuration("getCommand.ini");
		String temp=config.getValue("information");
		String str[]= temp.split(",");
		return str;
	}
	public String[] getEmail() throws ConfigurationException {
		config=new Configuration("getCommand.ini");
		String temp=config.getValue("email");
		String str[]= temp.split(",");
		return str;
	}
	public String[] getFile() throws ConfigurationException {
		config=new Configuration("getCommand.ini");
		String temp=config.getValue("file");
		String str[]= temp.split(",");
		return str;
	}
	public String[] getMessage() throws ConfigurationException {
		config=new Configuration("getCommand.ini");
		String temp=config.getValue("getMessage");
		String str[]= temp.split(",");
		return str;
	}
	public String[] getPublic() throws ConfigurationException {
		config=new Configuration("getCommand.ini");
		String temp=config.getValue("public");
		String str[]=temp.split(",");
		return str;
	}
	public String[] getHiddenlove() throws ConfigurationException {
		config=new Configuration("getCommand.ini");
		String temp=config.getValue("hiddenlove");
		String str[]=temp.split(",");
		return str;
	}
}
